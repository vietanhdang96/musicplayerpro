package app.music.model.entity

data class ThemeListContainer(var themeList: MutableList<Theme>?) : ListContainer<Theme>(themeList)
