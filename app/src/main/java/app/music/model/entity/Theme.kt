package app.music.model.entity

data class Theme(val mThemeTitle: String, val background: Int, var isSelected: Boolean,
                 val toolbarBackground: String, val toolbarDarkThemeTextColor: String,
                 val toolbarLightThemeTextColor: String)
