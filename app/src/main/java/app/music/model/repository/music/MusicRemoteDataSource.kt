package app.music.model.repository.music

import androidx.work.ExistingWorkPolicy
import androidx.work.WorkContinuation
import androidx.work.WorkManager
import app.music.utils.WorkerUtils
import app.music.utils.threadhandler.workmanager.*
import javax.inject.Inject

class MusicRemoteDataSource @Inject constructor() {

    fun getOnlineMusicLoadingChain(): WorkContinuation {
        return with(WorkerUtils) {
            WorkManager.getInstance()
                    .beginUniqueWork(
                            LOAD_ONLINE_MUSIC_WORK_NAME,
                            ExistingWorkPolicy.KEEP,
                            buildOneTimeWorkRequest<OnlineMusicWorker>())
                    .then(listOf(
                            buildOneTimeWorkRequest<OnlineAlbumListWorker>(),
                            buildOneTimeWorkRequest<OnlineArtistListWorker>(),
                            buildOneTimeWorkRequest<OnlineGenreListWorker>(),
                            buildOneTimeWorkRequest<OnlinePlaylistListWorker>()))
        }
    }

    companion object {

        private const val LOAD_ONLINE_MUSIC_WORK_NAME = "LOAD_ONLINE_MUSIC_WORK_NAME"
    }
}