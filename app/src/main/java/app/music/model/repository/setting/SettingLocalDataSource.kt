package app.music.model.repository.setting

import app.music.model.repository.theme.ThemeRepository
import javax.inject.Inject

class SettingLocalDataSource @Inject constructor(
        private val mThemeRepository: ThemeRepository) {

    fun getDarkModeEnabled(): Boolean = mThemeRepository.getDarkModeEnabled()

    fun setDarkModeEnabled(isDarkModeEnabled: Boolean) {
        mThemeRepository.setDarkModeEnabled(isDarkModeEnabled)
    }
}