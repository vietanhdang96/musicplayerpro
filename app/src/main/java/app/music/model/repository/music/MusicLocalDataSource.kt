package app.music.model.repository.music

import android.text.TextUtils
import androidx.work.ExistingWorkPolicy
import androidx.work.WorkContinuation
import androidx.work.WorkManager
import app.music.model.entity.BaseMusik
import app.music.model.entity.Music
import app.music.model.entity.OnlineMusik
import app.music.utils.ConstantUtil
import app.music.utils.WorkerUtils
import app.music.utils.favorite.FavoriteConstantUtils
import app.music.utils.musicloading.LoadMusicUtil
import app.music.utils.musicstate.MusicStateConstantUtils
import app.music.utils.sharepreferences.PreferencesHelper
import app.music.utils.threadhandler.workmanager.*
import com.google.gson.Gson
import com.google.gson.JsonParser
import java.util.*
import javax.inject.Inject

class MusicLocalDataSource @Inject constructor(
        private val mPreferencesHelper: PreferencesHelper, private val mGson: Gson) {

    fun getOfflineMusicLoadingChain(): WorkContinuation {
        return with(WorkerUtils) {
            WorkManager.getInstance()
                    .beginUniqueWork(
                            LOAD_OFFLINE_MUSIC_WORK_NAME,
                            ExistingWorkPolicy.KEEP,
                            buildOneTimeWorkRequest<OfflineMusicWorker>())
                    .then(listOf(
                            buildOneTimeWorkRequest<OfflineAlbumListWorker>(),
                            buildOneTimeWorkRequest<OfflineArtistListWorker>(),
                            buildOneTimeWorkRequest<OfflineGenreListWorker>(),
                            buildOneTimeWorkRequest<OfflinePlaylistListWorker>(),
                            buildOneTimeWorkRequest<OfflineFolderListWorker>(),
                            buildOneTimeWorkRequest<OfflineFavortieListWorker>()))
        }
    }

    fun editFavoriteList(musik: BaseMusik): Boolean {
        for (music in LoadMusicUtil.sFavoriteList) {
            if (music != null) {
                val musicLocation = music.location
                if (musicLocation != null && musicLocation == musik.location) {
                    LoadMusicUtil.sFavoriteList.remove(music)
                    return false
                }
            }
        }

        with(LoadMusicUtil.sFavoriteList) {
            add(musik)
            if (size > 20) {
                removeAt(0)
            }
        }
        saveFavoriteList()
        return true
    }

    private fun saveFavoriteList() {
        val json = mGson.toJson(LoadMusicUtil.sFavoriteList)
        with(mPreferencesHelper) {
            putString(FavoriteConstantUtils.PREF_FAVORITE_LIST, json)
            apply()
        }
    }

    fun getLastPlayedMusicList(): List<BaseMusik> {
        val valueString = mPreferencesHelper.getString(MusicStateConstantUtils.PREF_LAST_MUSIC_LIST)
        val dataList = ArrayList<BaseMusik>()
        if (TextUtils.isEmpty(valueString)) return dataList
        val musicDataArray = JsonParser().parse(valueString).asJsonArray
        if (musicDataArray == null || musicDataArray.size() < 1) return dataList

        musicDataArray.forEach { jsonElement ->
            jsonElement?.let {
                if (jsonElement.toString().contains(ConstantUtil.OFFLINE_MUSIC)) {
                    dataList.add(mGson.fromJson(jsonElement, Music::class.java))
                } else {
                    dataList.add(mGson.fromJson(jsonElement, OnlineMusik::class.java))
                }
            }
        }
        return dataList
    }

    fun getLastPlayedSong(): BaseMusik? {
        val lastMusicObject = mPreferencesHelper.getString(
                MusicStateConstantUtils.PREF_LAST_PLAYED_MUSIC_OBJECT)
        if (TextUtils.isEmpty(lastMusicObject)) return null
        return if (isLastPlayedSongAOnlineMusic()) {
            mGson.fromJson(lastMusicObject, OnlineMusik::class.java)
        } else {
            mGson.fromJson(lastMusicObject, Music::class.java)
        }
    }

    private fun isLastPlayedSongAOnlineMusic(): Boolean {
        val objectType = mPreferencesHelper.getString(
                MusicStateConstantUtils.PREF_LAST_PLAYED_MUSIC_OBJECT_TYPE)
        return objectType == MusicStateConstantUtils.PREF_LAST_PLAYED_MUSIC_OBJECT_TYPE_ONLINE_MUSIC
    }

    fun getLastCountTime(): Long {
        return mPreferencesHelper.getLong(MusicStateConstantUtils.PREF_LAST_COUNT_TIME)
    }

    companion object {

        private const val LOAD_OFFLINE_MUSIC_WORK_NAME = "LOAD_OFFLINE_MUSIC_WORK_NAME"
    }
}