package app.music.model.repository.music

import androidx.work.OneTimeWorkRequest
import androidx.work.WorkContinuation
import app.music.model.entity.BaseMusik
import javax.inject.Inject

class MusicRepository @Inject constructor(
        private val mMusicLocalDataSource: MusicLocalDataSource,
        private val mMusicRemoteDataSource: MusicRemoteDataSource) {

    fun getAllMusicList(oneTimeWorkRequest: OneTimeWorkRequest) {
        val onlineMusicChain = mMusicRemoteDataSource.getOnlineMusicLoadingChain()
        val offlineMusicChain = mMusicLocalDataSource.getOfflineMusicLoadingChain()
        WorkContinuation
                .combine(listOf(onlineMusicChain, offlineMusicChain))
                .then(oneTimeWorkRequest)
                .enqueue()
    }

    fun editFavoriteList(musik: BaseMusik): Boolean = mMusicLocalDataSource.editFavoriteList(musik)

    fun getLastPlayedMusicList(): List<BaseMusik> = mMusicLocalDataSource.getLastPlayedMusicList()

    fun getLastPlayedSong(): BaseMusik? = mMusicLocalDataSource.getLastPlayedSong()

    fun getLastCountTime(): Long = mMusicLocalDataSource.getLastCountTime()
}