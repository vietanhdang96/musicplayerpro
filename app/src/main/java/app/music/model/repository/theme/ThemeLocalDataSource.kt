package app.music.model.repository.theme

import android.app.Activity
import app.music.R
import app.music.base.activity.BaseActivity
import app.music.model.entity.Theme
import app.music.model.entity.ThemeListContainer
import app.music.utils.AppDataUtils
import app.music.utils.sharepreferences.PreferencesHelper
import app.music.utils.theme.ThemeConstantUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ThemeLocalDataSource @Inject constructor(
        private val mPreferencesHelper: PreferencesHelper) {

    fun loadThemeData(activity: Activity, loadThemeDataFinish: (Boolean) -> Unit) {
        var musicUpdateDisposable: Disposable? = null
        musicUpdateDisposable = Observable.defer { Observable.just("") }
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .map {
                    initThemeData()
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = {
                            loadThemeDataFinish(true)
                        },
                        onError = { it.printStackTrace() },
                        onComplete = {
                            musicUpdateDisposable?.dispose()
                            musicUpdateDisposable = null
                        }
                )
        (activity as BaseActivity<*>).mCompositeDisposable.add(musicUpdateDisposable!!)
    }

    private fun initThemeData() {
//        deleteThemeData()
        if (getThemeList().isEmpty()) {
            setDefaultThemeList()
            setDefaultThemeIndex()
            setDefaultDarkModeEnabled()
        } else {
            setThemeList()
            setThemeIndex()
            setDarkModeEnabled()
        }
    }

    fun getDarkModeEnabled(): Boolean {
        return AppDataUtils.sIsDarkModeEnabled
    }

    private fun setDefaultDarkModeEnabled() {
        AppDataUtils.sIsDarkModeEnabled = true
        saveDarkModeEnabled(true)
    }

    fun setDarkModeEnabled(isDarkModeEnabled: Boolean) {
        AppDataUtils.sIsDarkModeEnabled = isDarkModeEnabled
        saveDarkModeEnabled(isDarkModeEnabled)
    }

    private fun setDarkModeEnabled() {
        AppDataUtils.sIsDarkModeEnabled =
                mPreferencesHelper.getBoolean(ThemeConstantUtils.PREF_IS_DARK_MODE_ENABLED, true)
    }

    private fun saveDarkModeEnabled(isDarkModeEnabled: Boolean) {
        with(mPreferencesHelper) {
            putBoolean(ThemeConstantUtils.PREF_IS_DARK_MODE_ENABLED, isDarkModeEnabled)
            apply()
        }
    }

    private fun getThemeList(): MutableList<Theme> {
        return mPreferencesHelper.getList(
                ThemeListContainer::class.java,
                ThemeConstantUtils.PREF_THEME_LIST
        )
    }

    private fun getDefaultThemeList(): MutableList<Theme> {
        return mutableListOf(
                Theme("Maroon", R.drawable.bg_theme_item_maroon, true, "#80800000", "#ffffff", "#ffffff"),
                Theme("Dark Red", R.drawable.bg_theme_item_dark_red, false, "#808B0000", "#ffffff", "#ffffff"),
                Theme("Brown", R.drawable.bg_theme_item_brown, false, "#80A52A2A", "#ffffff", "#ffffff"),
                Theme("Deep Sky Blue", R.drawable.bg_theme_item_deep_sky_blue, false, "#8000BFFF", "#ffffff", "#000000"),
                Theme("Navy", R.drawable.bg_theme_item_navy, false, "#80000080", "#ffffff", "#ffffff"),
                Theme("Indigo", R.drawable.bg_theme_item_indigo, false, "#804B0082", "#ffffff", "#ffffff"),
                Theme("Magenta", R.drawable.bg_theme_item_magenta, false, "#80FF00FF", "#ffffff", "#000000"),
                Theme("Black", R.drawable.bg_theme_item_black, false, "#80000000", "#ffffff", "#ffffff")
        )
    }

    private fun setThemeList() {
        AppDataUtils.sThemeList = mPreferencesHelper.getList(
                ThemeListContainer::class.java,
                ThemeConstantUtils.PREF_THEME_LIST
        )
    }

    private fun setDefaultThemeList() {
        setThemeList(getDefaultThemeList())
        saveThemeList(AppDataUtils.sThemeList)
    }

    fun setThemeList(themeList: MutableList<Theme>) {
        with(AppDataUtils) {
            sThemeList = themeList
            saveThemeList(sThemeList)
        }
    }

    private fun saveThemeList(themeList: MutableList<Theme>) {
        with(mPreferencesHelper) {
            putList(ThemeConstantUtils.PREF_THEME_LIST, ThemeListContainer(themeList))
            apply()
        }
    }

    fun getThemeIndex(): Int = AppDataUtils.sThemeIndex

    private fun setThemeIndex() {
        AppDataUtils.sThemeIndex = mPreferencesHelper.getInt(ThemeConstantUtils.PREF_THEME_INDEX, 0)
    }

    private fun setDefaultThemeIndex() {
        setThemeIndex(0)
        saveThemeIndex(0)
    }

    fun setThemeIndex(themeIndex: Int) {
        with(AppDataUtils) {
            sThemeIndex = themeIndex
            saveThemeIndex(sThemeIndex)
        }
    }

    private fun saveThemeIndex(themeIndex: Int) {
        with(mPreferencesHelper) {
            putInt(ThemeConstantUtils.PREF_THEME_INDEX, themeIndex)
            apply()
        }
    }

    private fun deleteThemeData() {
        mPreferencesHelper.remove(ThemeConstantUtils.PREF_THEME_LIST)
    }
}