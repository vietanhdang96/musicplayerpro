package app.music.model.repository.setting

import javax.inject.Inject

class SettingRepository @Inject constructor(
        private val settingLocalDataSource: SettingLocalDataSource) {

    fun getDarkModeEnabled(): Boolean = settingLocalDataSource.getDarkModeEnabled()

    fun setDarkModeEnabled(isDarkModeEnabled: Boolean) {
        settingLocalDataSource.setDarkModeEnabled(isDarkModeEnabled)
    }
}