package app.music.model.repository.theme

import android.app.Activity
import app.music.model.entity.Theme
import javax.inject.Inject

class ThemeRepository @Inject constructor(
        private val mThemeLocalDataSource: ThemeLocalDataSource) {

    fun loadThemeData(activity: Activity, loadThemeDataFinish: (Boolean) -> Unit) {
        mThemeLocalDataSource.loadThemeData(activity, loadThemeDataFinish)
    }

    fun getDarkModeEnabled() = mThemeLocalDataSource.getDarkModeEnabled()

    fun setDarkModeEnabled(isDarkModeEnabled: Boolean) {
        mThemeLocalDataSource.setDarkModeEnabled(isDarkModeEnabled)
    }

    fun getThemeIndex(): Int = mThemeLocalDataSource.getThemeIndex()

    fun setThemeIndex(themeIndex: Int) {
        mThemeLocalDataSource.setThemeIndex(themeIndex)
    }

    fun setThemeList(themeList: MutableList<Theme>) {
        mThemeLocalDataSource.setThemeList(themeList)
    }
}