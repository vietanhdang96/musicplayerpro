package app.music.di.component;

import android.animation.ObjectAnimator;
import android.content.SharedPreferences;
import android.media.MediaMetadataRetriever;
import android.os.Handler;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;

import javax.inject.Singleton;

import app.music.base.BaseApplication;
import app.music.di.module.AppModule;
import app.music.di.module.ViewModelModule;
import dagger.Component;
import retrofit2.Retrofit;

@Singleton
@Component(modules = {AppModule.class, ViewModelModule.class})
public interface AppComponent {

    BaseApplication getContext();

    Handler getHandler();

    MediaMetadataRetriever getMediaMetadataRetriever();

    Gson getGson();

    SimpleDateFormat getSimpleDateFormat();

    Retrofit getRetrofit();

    ObjectAnimator getObjectAnimator();

    SharedPreferences getSharedPreferences();

    SharedPreferences.Editor getSharedPreferencesEditor();
}
