package app.music.di.component;

import android.app.Activity;

import androidx.fragment.app.Fragment;

import app.music.di.module.ActivityViewModelModule;
import app.music.di.module.FragmentModule;
import app.music.di.scope.FragmentScope;
import app.music.screen.album.AlbumFragment;
import app.music.screen.album.OnlineAlbumFragment;
import app.music.screen.artist.ArtistFragment;
import app.music.screen.artist.OnlineArtistFragment;
import app.music.screen.folder.FolderFragment;
import app.music.screen.genre.GenreFragment;
import app.music.screen.genre.OnlineGenreFragment;
import app.music.screen.playlist.OnlinePlaylistFragment;
import app.music.screen.playlist.PlaylistFragment;
import app.music.screen.song.OnlineSongFragment;
import app.music.screen.song.SongFragment;
import dagger.Component;

@FragmentScope
@Component(
        dependencies = AppComponent.class,
        modules = {FragmentModule.class, ActivityViewModelModule.class}
)
public interface FragmentComponent {

    Activity getActivity();

    Fragment getFragment();

    void inject(AlbumFragment albumFragment);

    void inject(OnlineAlbumFragment onlineAlbumFragment);

    void inject(SongFragment songFragment);

    void inject(OnlineSongFragment onlineSongFragment);

    void inject(ArtistFragment artistFragment);

    void inject(OnlineArtistFragment onlineArtistFragment);

    void inject(GenreFragment genreFragment);

    void inject(OnlineGenreFragment onlineGenreFragment);

    void inject(PlaylistFragment playlistFragment);

    void inject(OnlinePlaylistFragment onlinePlaylistFragment);

    void inject(FolderFragment folderFragment);
}
