package app.music.di.component;

import android.app.Activity;

import app.music.di.module.ActivityModule;
import app.music.di.module.ActivityViewModelModule;
import app.music.di.scope.ActivityScope;
import app.music.screen.choosetheme.ChooseThemeActivity;
import app.music.screen.detailalbum.DetailAlbumActivity;
import app.music.screen.detailartist.view.DetailArtistActivity;
import app.music.screen.detailfavorite.DetailFavoriteActivity;
import app.music.screen.detailfolder.DetailFolderActivity;
import app.music.screen.detailgenre.DetailGenreActivity;
import app.music.screen.detailplaylist.DetailOnlinePlaylistActivity;
import app.music.screen.detailplaylist.DetailPlaylistActivity;
import app.music.screen.detailplaymusic.view.DetailPlayMusicActivity;
import app.music.screen.offlinehome.HomeActivity;
import app.music.screen.onlinehome.OnlineHomeActivity;
import app.music.screen.setting.SettingActivity;
import app.music.screen.splash.SplashActivity;
import dagger.Component;

@ActivityScope
@Component(
        dependencies = AppComponent.class,
        modules = {ActivityModule.class, ActivityViewModelModule.class}
)
public interface ActivityComponent {

    Activity getActivity();

    void inject(SplashActivity splashActivity);

    void inject(DetailAlbumActivity detailAlbumActivity);

    void inject(DetailArtistActivity detailArtistActivity);

    void inject(DetailFolderActivity detailFolderActivity);

    void inject(DetailGenreActivity detailGenreActivity);

    void inject(DetailOnlinePlaylistActivity detailOnlinePlaylistActivity);

    void inject(DetailPlaylistActivity detailPlaylistActivity);

    void inject(HomeActivity homeActivity);

    void inject(OnlineHomeActivity onlineHomeActivity);

    void inject(ChooseThemeActivity chooseThemeActivity);

    void inject(SettingActivity settingActivity);

    void inject(DetailFavoriteActivity detailFavoriteActivity);

    void inject(DetailPlayMusicActivity detailPlayMusicActivity);
}
