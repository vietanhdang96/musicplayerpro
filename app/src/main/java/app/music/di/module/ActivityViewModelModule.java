package app.music.di.module;

import android.app.Activity;

import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import app.music.screen.offlinehome.HomeViewModel;
import app.music.screen.onlinehome.OnlineHomeViewModel;
import dagger.Module;
import dagger.Provides;

@Module
public class ActivityViewModelModule {

    @Provides
//    @ActivityScope
    static ViewModelProvider provideViewModelProvider(Activity activity) {
        return ViewModelProviders.of((FragmentActivity) activity);
    }

    @Provides
//    @ActivityScope
    static HomeViewModel provideHomeViewModel(ViewModelProvider viewModelProvider) {
        return viewModelProvider.get(HomeViewModel.class);
    }

    @Provides
//    @ActivityScope
    static OnlineHomeViewModel provideOnlineHomeViewModel(ViewModelProvider viewModelProvider) {
        return viewModelProvider.get(OnlineHomeViewModel.class);
    }
}
