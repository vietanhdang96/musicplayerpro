package app.music.di.module;

import android.app.Activity;
import android.content.Intent;

import app.music.di.scope.ActivityScope;
import app.music.service.MusicService;
import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    private Activity mActivity;

    public ActivityModule(Activity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityScope
    public Activity provideActivity() {
        return mActivity;
    }

    @Provides
    @ActivityScope
    public Intent provideIntent(Activity activity) {
        return new Intent(activity, MusicService.class);
    }
}
