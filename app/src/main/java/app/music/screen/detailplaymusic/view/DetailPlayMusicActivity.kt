package app.music.screen.detailplaymusic.view

import android.animation.ObjectAnimator
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.SeekBar
import androidx.lifecycle.Observer
import app.music.R
import app.music.base.musicservice.BaseMVVMPMusicActivity
import app.music.databinding.ActivityDetailPlayMusicBinding
import app.music.screen.detailplaymusic.DetailPlayMusicViewModel
import app.music.utils.intent.IntentMethodUtils
import app.music.utils.listener.DetailPlayMusicListener
import app.music.utils.toast.ToastUtil
import kotlinx.android.synthetic.main.activity_detail_play_music.*
import java.lang.ref.WeakReference
import java.text.SimpleDateFormat
import javax.inject.Inject

class DetailPlayMusicActivity
    : BaseMVVMPMusicActivity<ActivityDetailPlayMusicBinding, DetailPlayMusicViewModel>(),
        DetailPlayMusicListener,
        View.OnClickListener,
        SeekBar.OnSeekBarChangeListener {

    @Inject
    lateinit var mSimpleDateFormat: SimpleDateFormat
    @Inject
    lateinit var mObjectAnimator: ObjectAnimator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(mViewModel) {
            observeLiveData(
                    mRotateAnimationState,
                    Observer { rotateAnimationState ->
                        if (rotateAnimationState) {
                            resumeRotateAnimation()
                        } else {
                            pauseRotateAnimation()
                        }
                    }
            )
            observeLiveData(
                    mIsLaunchDetailPlayMvActivity,
                    Observer { isLaunchDetailPlayMvActivity ->
                        isLaunchDetailPlayMvActivity.getValueIfNotHandled()?.let {
                            IntentMethodUtils.launchDetailPlayMvActivity(
                                    this@DetailPlayMusicActivity,
                                    getLastContainingMVSong()!!
                            )
                        }
                    }
            )
        }
    }

    override fun onStart() {
        super.onStart()
        mViewModel.displayPlayingSongInfo(
                WeakReference(this),
                imageBlurBackGround,
                imageCenterCoverArt,
                animationViewFavorite
        )
    }

    override fun getLayoutId(): Int = R.layout.activity_detail_play_music

    override fun getLogTag(): String = TAG

    override fun getOptionMenuId(): Int = R.menu.activity_detail_play_music

    override fun createOptionMenu(menu: Menu) {

    }

    override fun initInject() = activityComponent.inject(this)

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.option_menu -> ToastUtil.showToast("menu clicked")
        }
        return super.onOptionsItemSelected(item)
    }

    override fun initView() {
        bindMusicService()
        setupActionBar()
        with(binding) {
            detailPlayMusicViewModel = mViewModel
            assignViews(btnShuffle, btnPrev, btnPlay, btnNext, btnReplay, imageLyrics,
                    imagePlayingList, animationViewFavorite, imageMusicVideo, imageVolume)
            songSeekBar.setOnSeekBarChangeListener(this@DetailPlayMusicActivity)
        }
        setCoverArtAnimation()
//        animation_view.addAnimatorUpdateListener { animation ->
//            var value: Float = animation.animatedValue.toString().toFloat()
//            if (value.compareTo(0.5f) == 1) {
//                imageFavorite.visibility = View.VISIBLE
//            } else {
//                imageFavorite.visibility = View.VISIBLE
//            }
//        }
    }

    override fun initData() {

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onClick(v: View) {
        with(mViewModel) {
            with(ToastUtil) {
                with(this@DetailPlayMusicActivity) {
                    when (v.id) {
                        R.id.btn_play -> changeExoPlayerState()
                        R.id.imageLyrics -> showToast("imageLyrics click")
                        R.id.imagePlayingList -> showToast("imagePlayingList click")
                        R.id.animationViewFavorite -> {
                            editFavoriteList(WeakReference(this), animationViewFavorite)
                        }
                        R.id.imageMusicVideo -> playMusicVideo()
                        R.id.imageVolume -> showToast("imageVolume click")
                        R.id.btn_prev -> playPreviousSong(this)
                        R.id.btn_next -> playNextSong(this)
                        R.id.btn_shuffle -> enableShuffleMode()
                        R.id.btn_replay -> enableReplayMode()
                    }
                }
            }
        }
    }

    override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
        mViewModel.setCountTime(mSimpleDateFormat.format(progress))
    }

    override fun onStartTrackingTouch(seekBar: SeekBar) {
        mViewModel.setSeeking(true)
    }

    override fun onStopTrackingTouch(seekBar: SeekBar) {
        with(mViewModel) {
            setSeeking(false)
            when (seekBar.id) {
                R.id.song_seek_bar -> {
                    mBinding.songSeekBar.progress.let { progress ->
                        setSeekBarProgress(progress)
                        seek(progress.toLong())
                    }
                }
            }
        }
    }

    override fun changePlayButtonImageResource(resId: Int) = mBinding.btnPlay.setImageResource(resId)

    override fun changeExoPlayerState() = mViewModel.changeExoPlayerState(this)

    private fun pauseRotateAnimation() = mObjectAnimator.pause()

    private fun resumeRotateAnimation() = mObjectAnimator.resume()

    private fun setupActionBar() {
        setSupportActionBar(mBinding.toolbarTitle)
        supportActionBar?.run {
            setDisplayHomeAsUpEnabled(true)
            elevation = 0f
        }
    }

    private fun assignViews(vararg views: View) = views.forEach { it.setOnClickListener(this) }

    private fun setCoverArtAnimation() {
        with(mObjectAnimator) {
            target = imageCenterCoverArt
            start()
        }
    }

    companion object {

        private const val TAG = "DetailPlayMusicActivity"
    }
}
