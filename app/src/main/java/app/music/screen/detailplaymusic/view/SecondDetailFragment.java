package app.music.screen.detailplaymusic.view;


import android.animation.ObjectAnimator;
import android.content.Context;

import com.bumptech.glide.request.RequestOptions;

import app.music.R;
import app.music.base.BaseFragment;
import app.music.databinding.FragmentSecondDetailBinding;
import app.music.utils.listener.DetailPlayMusicListener;

public class SecondDetailFragment
        extends BaseFragment<FragmentSecondDetailBinding> {

    private static final String TAG = "SecondDetailFragment";
    private ObjectAnimator mAnim;
    private DetailPlayMusicListener mDetailPlayMusicListener;
    private RequestOptions mRequestOptions = new RequestOptions()
            .error(R.drawable.ic_album);

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DetailPlayMusicListener) {
            mDetailPlayMusicListener = (DetailPlayMusicListener) context;
        }
    }

    @Override
    protected void initInject() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_second_detail;
    }

    @Override
    public String getLogTag() {
        return TAG;
    }

    @Override
    public void initView() {

    }

    @Override
    public void initData() {

    }
}
