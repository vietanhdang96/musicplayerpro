package app.music.screen.detailplaymusic

import android.content.Context
import android.widget.ImageView
import app.music.base.musicservice.MVVMPMusicContract
import app.music.model.entity.BaseMusik
import com.airbnb.lottie.LottieAnimationView
import io.github.armcha.coloredshadow.ShadowImageView
import java.lang.ref.WeakReference

interface DetailPlayMusicContract {

    interface ViewModel : MVVMPMusicContract.ViewModel {

        fun setRotateAnimationState(rotateAnimationState: Boolean)

        fun setLastContainingMVSong(lastContainingMVSong: BaseMusik)

        fun setLaunchDetailPlayMvActivity(launchDetailPlayMvActivity: Boolean)

        fun getSeeking(): Boolean

        fun setSongTotalTime(songTotalTime: String)

        fun setFavorite(isFavorite: Boolean)

        fun setReplay(isReplay: Boolean)

        fun setShuffle(isShuffle: Boolean)
    }

    interface Presenter : MVVMPMusicContract.Presenter {

        fun enableShuffleMode()

        fun enableReplayMode()

        fun seekToPosition(seekPosition: Long)

        fun displayPlayingSongInfo(
                contextReference: WeakReference<Context>, backgroundImage: ImageView,
                centerCoverImage: ShadowImageView, animationView: LottieAnimationView)

        fun playMusicVideo()

        fun editFavoriteList(contextReference: WeakReference<Context>, animationView: LottieAnimationView)
    }
}