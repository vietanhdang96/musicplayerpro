package app.music.screen.detailplaymusic

import android.content.Context
import android.widget.ImageView
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import app.music.base.musicservice.BaseMVVMPMusicViewModel
import app.music.model.entity.BaseMusik
import app.music.utils.livedata.HandleOnceLiveEvent
import com.airbnb.lottie.LottieAnimationView
import io.github.armcha.coloredshadow.ShadowImageView
import java.lang.ref.WeakReference
import javax.inject.Inject

class DetailPlayMusicViewModel @Inject constructor(
        detailPlayMusicPresenter: DetailPlayMusicPresenter)
    : BaseMVVMPMusicViewModel<DetailPlayMusicPresenter>(detailPlayMusicPresenter),
        DetailPlayMusicContract.ViewModel {

    var mIsFavorite = ObservableBoolean()
    var mIsReplay = ObservableBoolean()
    var mIsShuffle = ObservableBoolean()
    var mSongTotalTime = ObservableField<String>()
    var mCountTime = ObservableField<String>()
    private var mIsSeeking = false
    var mRotateAnimationState = MutableLiveData(false)
    var mIsLaunchDetailPlayMvActivity = MutableLiveData<HandleOnceLiveEvent<Boolean>>()
    private var mLastContainingMVSong = MutableLiveData<BaseMusik>()

    override fun setRotateAnimationState(rotateAnimationState: Boolean) {
        mRotateAnimationState.value = rotateAnimationState
    }

    override fun setLaunchDetailPlayMvActivity(launchDetailPlayMvActivity: Boolean) {
        mIsLaunchDetailPlayMvActivity.value = HandleOnceLiveEvent(launchDetailPlayMvActivity)
    }

    override fun setLastContainingMVSong(lastContainingMVSong: BaseMusik) {
        mLastContainingMVSong.value = lastContainingMVSong
    }

    override fun setSongTotalTime(songTotalTime: String) {
        mSongTotalTime.set(songTotalTime)
    }

    override fun setFavorite(isFavorite: Boolean) {
        mIsFavorite.set(isFavorite)
    }

    override fun setReplay(isReplay: Boolean) {
        mIsReplay.set(isReplay)
    }

    override fun setShuffle(isShuffle: Boolean) {
        mIsShuffle.set(isShuffle)
    }

    override fun getSeeking(): Boolean {
        return mIsSeeking
    }

    fun enableShuffleMode() {
        mPresenter.enableShuffleMode()
    }

    fun enableReplayMode() {
        mPresenter.enableReplayMode()
    }

    fun seek(seekPosition: Long) {
        mPresenter.seekToPosition(seekPosition)
    }

    fun displayPlayingSongInfo(
            contextReference: WeakReference<Context>, backgroundImage: ImageView,
            centerCoverImage: ShadowImageView, animationView: LottieAnimationView) {
        mPresenter.displayPlayingSongInfo(
                contextReference,
                backgroundImage,
                centerCoverImage,
                animationView
        )
    }

    fun setSeeking(isSeeking: Boolean) {
        mIsSeeking = isSeeking
    }

    fun playMusicVideo() {
        mPresenter.playMusicVideo()
    }

    fun editFavoriteList(contextReference: WeakReference<Context>, animationView: LottieAnimationView) {
        mPresenter.editFavoriteList(contextReference, animationView)
    }

    fun playPreviousSong(context: Context) {
        mPresenter.playPreviousSong(context)
    }

    fun playNextSong(context: Context) {
        mPresenter.playNextSong(context)
    }

    fun changeExoPlayerState(context: Context) {
        mPresenter.changeExoPlayerState(context)
    }

    fun getLastContainingMVSong(): BaseMusik? {
        return mLastContainingMVSong.value
    }

    fun setCountTime(countTime: String) {
        mCountTime.set(countTime)
    }
}