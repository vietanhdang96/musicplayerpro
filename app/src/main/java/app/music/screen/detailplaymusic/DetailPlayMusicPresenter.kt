package app.music.screen.detailplaymusic

import android.app.Activity
import android.content.Context
import android.text.TextUtils
import android.widget.ImageView
import app.music.R
import app.music.base.musicservice.BaseMVVMPMusicPresenter
import app.music.model.entity.BaseMusik
import app.music.model.entity.OnlineMusik
import app.music.model.repository.music.MusicRepository
import app.music.utils.ConstantUtil
import app.music.utils.blur.BlurImageUtils
import app.music.utils.imageloading.ImageLoadingUtils
import app.music.utils.musicloading.LoadMusicUtil
import app.music.utils.toast.ToastUtil
import com.airbnb.lottie.LottieAnimationView
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import io.github.armcha.coloredshadow.ShadowImageView
import java.lang.ref.WeakReference
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class DetailPlayMusicPresenter @Inject constructor(
        private val mMusicRepository: MusicRepository, private val mSimpleDateFormat: SimpleDateFormat)
    : BaseMVVMPMusicPresenter<DetailPlayMusicViewModel>(),
        DetailPlayMusicContract.Presenter {

    private val mBlurBackgroundRequestOptions = RequestOptions().error(R.drawable.bg_main_color)
    private val mCenterCoverArtRequestOptions = RequestOptions()
            .error(R.drawable.ic_disc)
            .transform(MultiTransformation(CircleCrop(), CenterCrop()))

    override fun editFavoriteList(contextReference: WeakReference<Context>, animationView: LottieAnimationView) {
        mMusicService?.run {
            if (isRunning) {
                editFavoriteList(playingSong, animationView)
            } else {
                for (music in mLastMusicList) {
                    if (music.location == mLastPlayedMusicObject!!.location) {
                        editFavoriteList(music, animationView)
                        break
                    }
                }
            }
        }
    }

    override fun displayPlayingSongInfo(
            contextReference: WeakReference<Context>, backgroundImage: ImageView,
            centerCoverImage: ShadowImageView, animationView: LottieAnimationView) {
        displayPlayingSongInfo(
                {
                    pauseRotateAnimation()
                    pauseMusic()
                },
                {
                    loadLastState(contextReference, backgroundImage, centerCoverImage, animationView)
                    setButtonImageResource()
                },
                {
                    updateSongInfo(contextReference, backgroundImage, centerCoverImage, animationView)
                },
                ::displayRotateAnimation,
                { setButtonImageResource() },
                ::setSeekBarProgress
        )
    }

    private fun displayRotateAnimation(isPlaying: Boolean) {
        if (isPlaying) {
            // resume animation when a song is playing
            resumeRotateAnimation()
        } else {
            // pause animation when a song is pausing
            pauseRotateAnimation()
        }
    }

    fun changeExoPlayerState(context: Context) {
        changeExoPlayerState(
                context,
                {
                    pauseRotateAnimation()
                    pauseMusic()
                },
                {
                    startService(context)
                    resumeRotateAnimation()
                    playMusic()
                },
                { mViewModel?.getSeekBarProgress()?.toLong()!! },
                ::setReplayAndShuffleImageResource
        )
    }

    override fun enableShuffleMode() {
        mMusicService?.run {
            shuffleState = !shuffleState
            replayState = false
            if (isRunning) {
                setPlayerShuffleMode()
            }
            setReplayAndShuffleImageResource()
        }
    }

    override fun enableReplayMode() {
        mMusicService?.run {
            replayState = !replayState
            shuffleState = false
            if (isRunning) {
                setPlayerRepeatMode()
            }
            setReplayAndShuffleImageResource()
        }
    }

    override fun seekToPosition(seekPosition: Long) {
        mMusicService?.run {
            if (isRunning && mMusicBound) {
                seek(seekPosition)
            }
        }
    }

    override fun playMusicVideo() {
        val music: BaseMusik? = if (mMusicService != null && mMusicService!!.isRunning) {
            mMusicService!!.playingSong
        } else {
            mLastPlayedMusicObject
        }
        if (music != null) {
            val musicType = music.type
            if (!TextUtils.isEmpty(musicType)) {
                with(ToastUtil) {
                    when (musicType) {
                        ConstantUtil.OFFLINE_MUSIC -> showToast("MV is not available for this song")
                        ConstantUtil.ONLINE_MUSIC -> {
                            val mvLink = (music as OnlineMusik).mvLink
                            if (!TextUtils.isEmpty(mvLink)) {
                                mMusicService?.run {
                                    if (isRunning) {
                                        stopForeground()
                                    }
                                }
                                mViewModel?.run {
                                    setLastContainingMVSong(music)
                                    setLaunchDetailPlayMvActivity(true)
                                }
                            } else {
                                showToast("MV is not available for this song")
                            }
                        }
                        else -> {
                        }
                    }
                }
            }
        }
    }

    private fun hideOutlineFavoriteIcon() {
        mViewModel?.setFavorite(true)
    }

    private fun showOutlineFavoriteIcon() {
        mViewModel?.setFavorite(false)
    }

    private fun updateFavoriteStatus(animationView: LottieAnimationView) {
        if (mMusicService != null && mMusicService?.isRunning!!) {
            setFavoriteStatus(mMusicService!!.playingSong.location, animationView)
        } else {
            for (music in mLastMusicList) {
                if (music.location == mLastPlayedMusicObject!!.location) {
                    setFavoriteStatus(music.location, animationView)
                }
            }
        }
    }

    private fun setFavoriteStatus(musicLocation: String, animationView: LottieAnimationView) {
        if (TextUtils.isEmpty(musicLocation)) return
        showOutlineFavoriteIcon()
        setFavoriteAnimationProgress(0.0f, animationView)
        for (musik in LoadMusicUtil.sFavoriteList) {
            if (musik.location == musicLocation) {
                hideOutlineFavoriteIcon()
                setFavoriteAnimationProgress(1.0f, animationView)
                return
            }
        }
    }

    private fun setSeekBarProgress(barProgress: Int) {
        mViewModel?.run {
            if (!getSeeking()) {
                setSeekBarProgress(barProgress)
            }
        }
    }

    private fun updateSongInfo(
            activityReference: WeakReference<Context>, backgroundImage: ImageView,
            centerCoverImage: ShadowImageView, animationView: LottieAnimationView) {
        val music = mMusicService?.playingSong
        setBackgroundAndCoverArt(music, activityReference, backgroundImage, centerCoverImage)
        updateFavoriteStatus(animationView)
        updateLyrics(music)
        setTotalTime(music)
        setSongTitle(music)
        setButtonImageResource()
    }

    private fun setSongTitle(music: BaseMusik?) {
        if (music == null) return
        val musicTitle = music.title
        val musicArtist = music.artist
        if (TextUtils.isEmpty(musicTitle) || TextUtils.isEmpty(musicArtist)) return
        mViewModel?.run {
            setSongTitle(music.title)
            setArtistName(music.artist)
        }
    }

    private fun setTotalTime(music: BaseMusik?) {
        if (music == null) return
        val duration = music.duration!!
        mViewModel?.run {
            setSongTotalTime(mSimpleDateFormat.format(duration))
            setSeekBarMax(duration)
        }
    }

    private fun editFavoriteList(musik: BaseMusik, animationView: LottieAnimationView) {
        val isFavoriteSong = mMusicRepository.editFavoriteList(musik)
        playFavoriteAnimation(isFavoriteSong, animationView)
    }

    private fun setFavoriteAnimationProgress(progress: Float, animationView: LottieAnimationView) {
        animationView.progress = progress
    }

    private fun playFavoriteAnimation(isFavoriteSong: Boolean, animationView: LottieAnimationView) {
        if (isFavoriteSong) {
            hideOutlineFavoriteIcon()
            animationView.playAnimation()
        } else {
            showOutlineFavoriteIcon()
            setFavoriteAnimationProgress(0.0f, animationView)
        }
    }

    private fun pauseMusic() {
        mMusicService?.exoPlayer?.playWhenReady = false
        setPlayButtonImageResource()
    }

    private fun playMusic() {
        mMusicService?.run {
            addPlayerListener()
            exoPlayer?.playWhenReady = true
        }
        setPlayButtonImageResource()
    }

    private fun updateLyrics(music: BaseMusik?) {
        if (music == null) return
        val lyrics = music.lyrics
    }

    private fun setBackgroundAndCoverArt(
            music: BaseMusik?, activityReference: WeakReference<Context>, backgroundImage: ImageView,
            centerCoverImage: ShadowImageView) {
        if (music == null) return
        val musicType = music.type
        if (TextUtils.isEmpty(musicType)) return
        when (musicType) {
            ConstantUtil.OFFLINE_MUSIC -> {
                with(mMetadataRetriever) {
                    setDataSource(music.location)
                    val bytes = embeddedPicture
                    if (bytes != null) {
                        BlurImageUtils.blurImage(
                                activityReference.get() as Activity,
                                bytes,
                                backgroundImage
                        )
                    } else {
                        loadDefaultBackGround(backgroundImage)
                    }
                    ImageLoadingUtils.loadShadowedImage(
                            centerCoverImage,
                            bytes,
                            mCenterCoverArtRequestOptions
                    )
                }
            }
            ConstantUtil.ONLINE_MUSIC -> {
                val coverArtLink = (music as OnlineMusik).coverArt
                if (!TextUtils.isEmpty(coverArtLink)) {
                    BlurImageUtils.blurImage(
                            activityReference.get() as Activity,
                            coverArtLink,
                            mBlurBackgroundRequestOptions,
                            backgroundImage
                    )
                } else {
                    loadDefaultBackGround(backgroundImage)
                }
                ImageLoadingUtils.loadShadowedImage(
                        centerCoverImage,
                        coverArtLink,
                        mCenterCoverArtRequestOptions
                )
            }
            else -> {
            }
        }
    }

    private fun loadDefaultBackGround(imageView: ImageView) {
        ImageLoadingUtils.loadImage(imageView, R.drawable.bg_main_color)
    }

    private fun setButtonImageResource() {
        setPlayButtonImageResource()
        setReplayAndShuffleImageResource()
    }

    private fun setReplayAndShuffleImageResource() {
        if (mMusicService == null) return
        mViewModel?.run {
            mMusicService?.run {
                setReplay(replayState)
                setShuffle(shuffleState)
            }
        }
    }

    private fun setPlayButtonImageResource() {
        mMusicService?.run {
            exoPlayer?.run {
                mViewModel?.setPlaying(playWhenReady)
                return
            }
        }
        mViewModel?.setPlaying(false)
    }

    private fun loadLastState(
            contextReference: WeakReference<Context>, backgroundImage: ImageView,
            centerCoverImage: ShadowImageView, animationView: LottieAnimationView) {
        val lastCountTime: Long
        with(mMusicRepository) {
            val dataList = ArrayList(getLastPlayedMusicList())
            if (dataList.size > 0) {
                mLastMusicList = ArrayList(dataList)
                mLastPlayedMusicObject = getLastPlayedSong() ?: LoadMusicUtil.sMusicList[0]
                lastCountTime = getLastCountTime()
            } else {
                mLastMusicList = LoadMusicUtil.sMusicList
                mLastPlayedMusicObject = LoadMusicUtil.sMusicList[0]
                lastCountTime = 0
            }
        }

        val music = mLastPlayedMusicObject
        setBackgroundAndCoverArt(music, contextReference, backgroundImage, centerCoverImage)
        updateFavoriteStatus(animationView)
        updateLyrics(music)
        val duration = music!!.duration!!
        mViewModel?.run {
            setSongTotalTime(mSimpleDateFormat.format(duration))
            setSeekBarMax(duration)
            setPlaying(false)
        }
        setSeekBarProgress(lastCountTime.toInt())
        setSongTitle(music)
        pauseRotateAnimation()
    }

    private fun pauseRotateAnimation() {
        mViewModel?.setRotateAnimationState(false)
    }

    private fun resumeRotateAnimation() {
        mViewModel?.setRotateAnimationState(true)
    }
}
