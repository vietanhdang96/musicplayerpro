package app.music.screen.choosetheme

import android.graphics.Color
import app.music.R
import app.music.base.mvvmp.BaseMVVMPPresenter
import app.music.model.entity.Theme
import app.music.model.repository.theme.ThemeRepository
import javax.inject.Inject

class ChooseThemePresenter @Inject constructor(
        private val mThemeRepository: ThemeRepository)
    : BaseMVVMPPresenter<ChooseThemeViewModel>(),
        ChooseThemeContract.Presenter {

    override fun changeTheme(selectedTheme: Theme) {
        mViewModel?.run {
            with(selectedTheme) {
                setToolbarBackground(Color.parseColor(toolbarBackground))
                if (mThemeRepository.getDarkModeEnabled()) {
                    setToolbarTextColor(Color.parseColor(toolbarDarkThemeTextColor))
                    setIconBackResource(toolbarDarkThemeTextColor)
                } else {
                    setToolbarTextColor(Color.parseColor(toolbarLightThemeTextColor))
                    setIconBackResource(toolbarLightThemeTextColor)
                }
            }
        }
    }

    override fun setThemeIndex(themeIndex: Int) {
        mThemeRepository.setThemeIndex(themeIndex)
    }

    override fun getThemeIndex(): Int {
        return mThemeRepository.getThemeIndex()
    }

    fun setThemeList(themeList: MutableList<Theme>) {
        mThemeRepository.setThemeList(themeList)
    }

    fun setIconBackResource(toolbarDarkThemeTextColor: String,
                            toolbarLightThemeTextColor: String) {
        mViewModel?.setIconBackResource(
                getIconBackResource(
                        if (mThemeRepository.getDarkModeEnabled()) {
                            toolbarDarkThemeTextColor
                        } else {
                            toolbarLightThemeTextColor
                        }
                )
        )
    }

    private fun setIconBackResource(toolbarTextColor: String) {
        mViewModel?.setIconBackResource(getIconBackResource(toolbarTextColor))
    }

    private fun getIconBackResource(toolbarTextColor: String): Int {
        return if (toolbarTextColor == "#ffffff") {
            R.drawable.ic_back_white
        } else {
            R.drawable.ic_back_black
        }
    }
}
