package app.music.screen.choosetheme

import app.music.base.mvvmp.MVVMPContract
import app.music.model.entity.Theme

interface ChooseThemeContract {

    interface ViewModel : MVVMPContract.ViewModel

    interface Presenter : MVVMPContract.Presenter {

        fun setThemeIndex(themeIndex: Int)

        fun getThemeIndex(): Int

        fun changeTheme(selectedTheme: Theme)
    }
}