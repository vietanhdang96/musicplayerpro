package app.music.screen.choosetheme

import android.view.Menu
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import app.music.R
import app.music.base.mvvmp.BaseMVVMPActivity
import app.music.databinding.ActivityChooseThemeBinding
import app.music.model.entity.Theme
import app.music.utils.AppDataUtils
import app.music.utils.adapter.recycler.ThemeAdapter
import app.music.utils.blur.DynamicBlurUtils
import app.music.utils.color.AttributeColorUtils
import kotlinx.android.synthetic.main.activity_choose_theme.*
import java.lang.ref.WeakReference

class ChooseThemeActivity
    : BaseMVVMPActivity<ActivityChooseThemeBinding, ChooseThemeViewModel>() {

    override fun getLayoutId() = R.layout.activity_choose_theme

    override fun initView() {
        with(binding) {
            with(mViewModel) {
                with(this@ChooseThemeActivity) {
                    DynamicBlurUtils.blurView(this, R.id.coordinator_background, blurToolBar)
                    setToolbarBackground(getAttributeColor(R.attr.main_transparent_color))
                    setToolbarTextColor(getAttributeColor(R.attr.toolbar_text_color))
                }
            }
            chooseThemeViewModel = mViewModel
        }
        setupActionBar()
        setThemeListData()
    }

    override fun initData() {

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun initInject() = activityComponent.inject(this)

    override fun getLogTag() = TAG

    override fun getOptionMenuId() = 0

    override fun createOptionMenu(menu: Menu) {

    }

    private fun getAttributeColor(attributeColorId: Int): Int {
        return AttributeColorUtils.getColor(this, attributeColorId)
    }

    private fun setIconBackResource(toolbarDarkThemeTextColor: String,
                                    toolbarLightThemeTextColor: String) {
        mViewModel.setIconBackResource(toolbarDarkThemeTextColor, toolbarLightThemeTextColor)
    }

    private fun setThemeListData() {
        with(recyclerTheme) {
            with(this@ChooseThemeActivity) {
                layoutManager = LinearLayoutManager(this)
                addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
                adapter = ThemeAdapter(
                        WeakReference(this),
                        AppDataUtils.sThemeList,
                        AppDataUtils.sThemeIndex,
                        ::setThemeList,
                        ::setThemeIndex,
                        ::setIconBackResource
                )
            }
        }
//        RecyclerViewUtils.setVerticalLinearLayout(binding.recyclerview, context!!, true, true)
    }

    private fun setThemeList(themeList: MutableList<Theme>, selectedTheme: Theme) {
        with(mViewModel) {
            setThemeList(themeList)
            changeTheme(selectedTheme)
        }
    }

    private fun setThemeIndex(themeIndex: Int) {
        mViewModel.setThemeIndex(themeIndex)
    }

    private fun setupActionBar() {
        setSupportActionBar(tool_bar)
        supportActionBar?.run {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowTitleEnabled(false)
        }
    }

    companion object {

        private const val TAG = "ChooseThemeActivity"
    }
}
