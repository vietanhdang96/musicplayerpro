package app.music.screen.choosetheme

import androidx.databinding.ObservableInt
import app.music.R
import app.music.base.mvvmp.BaseMVVMPViewModel
import app.music.model.entity.Theme
import javax.inject.Inject

class ChooseThemeViewModel @Inject constructor(
        chooseThemePresenter: ChooseThemePresenter)
    : BaseMVVMPViewModel<ChooseThemePresenter>(chooseThemePresenter),
        ChooseThemeContract.ViewModel {

    var mToolbarBackground = ObservableInt()
    val mToolbarTextColor = ObservableInt()
    var mIconBackResource = ObservableInt(R.drawable.ic_back_transparent)

    fun setIconBackResource(iconBackResource: Int) {
        mIconBackResource.set(iconBackResource)
    }

    fun setIconBackResource(toolbarDarkThemeTextColor: String,
                            toolbarLightThemeTextColor: String) {
        mPresenter?.setIconBackResource(toolbarDarkThemeTextColor, toolbarLightThemeTextColor)
    }

    fun setToolbarBackground(toolbarBackground: Int) {
        mToolbarBackground.set(toolbarBackground)
    }

    fun setToolbarTextColor(toolbarTextColor: Int) {
        mToolbarTextColor.set(toolbarTextColor)
    }

    fun changeTheme(selectedTheme: Theme) {
        mPresenter.changeTheme(selectedTheme)
    }

    fun setThemeList(themeList: MutableList<Theme>) {
        mPresenter.setThemeList(themeList)
    }

    fun setThemeIndex(themeIndex: Int) {
        mPresenter.setThemeIndex(themeIndex)
    }
}