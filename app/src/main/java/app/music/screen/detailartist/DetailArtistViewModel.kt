package app.music.screen.detailartist

import android.content.Context
import android.content.Intent
import android.widget.ImageView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.MutableLiveData
import app.music.base.bottomprogress.BaseMVVMPBottomProgressViewModel
import app.music.model.entity.Artist
import app.music.model.entity.BaseMusik
import javax.inject.Inject

class DetailArtistViewModel @Inject constructor(presenter: DetailArtistPresenter)
    : BaseMVVMPBottomProgressViewModel<DetailArtistPresenter>(presenter),
        DetailArtistContract.ViewModel {

    var mArtist = MutableLiveData<Artist>()

    override fun setArtist(artist: Artist) {
        mArtist.value = artist
    }

    fun getArtist(): Artist? = mArtist.value

    fun displayData(context: Context, intent: Intent?, backgroundLayout: CoordinatorLayout,
                    coverImage: ImageView) {
        mPresenter.displayData(context, intent, backgroundLayout, coverImage)
    }

    fun playPickedSong(musicList: MutableList<BaseMusik>, pickedSong: BaseMusik,
                       coverImage: ImageView) {
        mPresenter.playPickedSong(musicList, pickedSong, coverImage)
    }
}