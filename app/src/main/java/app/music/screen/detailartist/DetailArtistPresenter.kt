package app.music.screen.detailartist

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.text.TextUtils
import android.widget.ImageView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import app.music.R
import app.music.base.bottomprogress.BaseMVVMPBottomProgressPresenter
import app.music.model.entity.Artist
import app.music.model.entity.BaseMusik
import app.music.model.entity.OnlineMusik
import app.music.model.repository.music.MusicRepository
import app.music.utils.ConstantUtil
import app.music.utils.gradient.GradientBackgroundUtils
import app.music.utils.imageloading.ImageLoadingUtils
import app.music.utils.intent.IntentConstantUtils
import com.bumptech.glide.request.RequestOptions
import java.util.*
import javax.inject.Inject

class DetailArtistPresenter @Inject constructor(musicRepository: MusicRepository)
    : BaseMVVMPBottomProgressPresenter<DetailArtistViewModel>(musicRepository),
        DetailArtistContract.Presenter {

    private var mMusicList: List<BaseMusik> = ArrayList()
    private val mRequestOptions = RequestOptions().error(R.drawable.ic_album)

    override fun displayData(context: Context, intent: Intent?, backgroundLayout: CoordinatorLayout,
                             coverImage: ImageView) {
        intent?.run {
            val artist: Artist? = getParcelableExtra(IntentConstantUtils.EXTRA_ARTIST_OBJECT_TO_DETAIL_ARTIST)
            artist?.run {
                mViewModel?.setArtist(artist)
                mMusicList = ArrayList(musicList)
            }
            val music = mMusicList[0]
            with(music) {
                val musicType = type
                if (TextUtils.isEmpty(musicType)) return
                when (musicType) {
                    ConstantUtil.OFFLINE_MUSIC -> {
                        val songLocation = location
                        if (!TextUtils.isEmpty(songLocation)) {
                            with(mMetadataRetriever) {
                                setDataSource(songLocation)
                                val bytes = embeddedPicture
                                if (bytes != null) {
                                    GradientBackgroundUtils.createGradientBackground(
                                            context as Activity,
                                            bytes,
                                            backgroundLayout
                                    )
                                }
                                ImageLoadingUtils.loadImage(coverImage, bytes, mRequestOptions)
                            }
                        }
                    }
                    ConstantUtil.ONLINE_MUSIC -> {
                        val coverArtLink = (music as OnlineMusik).coverArt
                        if (!TextUtils.isEmpty(coverArtLink)) {
                            GradientBackgroundUtils.createGradientBackground(
                                    context as Activity,
                                    coverArtLink,
                                    backgroundLayout
                            )
                        }
                        ImageLoadingUtils.loadImage(coverImage, coverArtLink, mRequestOptions)
                    }
                    else -> {
                    }
                }
            }
        }
    }

    override fun playPickedSong(musicList: MutableList<BaseMusik>, pickedSong: BaseMusik,
                                coverImage: ImageView) {
        mMusicService?.setList(musicList)
        playPickedSong(pickedSong, coverImage)
    }
}