package app.music.screen.detailartist

import android.content.Context
import android.content.Intent
import android.widget.ImageView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import app.music.base.bottomprogress.MVVMPBottomProgressContract
import app.music.model.entity.Artist
import app.music.model.entity.BaseMusik

interface DetailArtistContract {

    interface ViewModel : MVVMPBottomProgressContract.ViewModel {

        fun setArtist(artist: Artist)
    }

    interface Presenter : MVVMPBottomProgressContract.Presenter {

        fun displayData(context: Context, intent: Intent?, backgroundLayout: CoordinatorLayout,
                        coverImage: ImageView)

        fun playPickedSong(musicList: MutableList<BaseMusik>, pickedSong: BaseMusik,
                           coverImage: ImageView)
    }
}
