package app.music.screen.detailartist.view


import android.app.Activity
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import app.music.R
import app.music.base.BaseFragment
import app.music.databinding.FragmentDetailArtistAlbumBinding
import app.music.model.entity.Album
import app.music.model.entity.Artist
import app.music.screen.detailartist.DetailArtistViewModel
import app.music.utils.ConstantUtil
import app.music.utils.adapter.recycler.ArtistAlbumAdapter
import app.music.utils.comparator.comparatorascending.album.AlbumComparatorByAlphabetAscending
import app.music.utils.listener.RecyclerScrollToTopListener
import app.music.utils.listener.itemclick.AlbumFragmentItemClickListener
import app.music.utils.musicloading.LoadMusicUtil
import java.lang.ref.WeakReference
import java.util.*
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 */
class DetailArtistAlbumFragment
    : BaseFragment<FragmentDetailArtistAlbumBinding>(),
        RecyclerScrollToTopListener,
        AlbumFragmentItemClickListener {

    private var mArtist: Artist? = null
    private var mRecyclerAdapter: ArtistAlbumAdapter? = null
    private val albumList = ArrayList<Album>()
    private var albumCount = 0
    private lateinit var mDetailArtistViewModel: DetailArtistViewModel

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val activity = activity
        if (activity != null) {
            (activity as DetailArtistActivity).mDetailArtistAlbumFragment = this
            mDetailArtistViewModel = activity.getViewModel()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mDetailArtistViewModel.mArtist.observe(
                this,
                Observer { artist ->
                    mArtist = artist
                    val albumNameList = mArtist!!.albumNameList
                    val music = mArtist!!.musicList[0]
                    when (music.type) {
                        ConstantUtil.OFFLINE_MUSIC -> {
                            addAlbumData(albumNameList, LoadMusicUtil.sAlbumList)
                        }
                        else -> {
                            addAlbumData(albumNameList, LoadMusicUtil.sOnlineAlbumList)
                        }
                    }
                    Collections.sort(albumList, AlbumComparatorByAlphabetAscending())
                    mRecyclerAdapter!!.updateItems(false, albumList)
                }
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.recycler.adapter = null
    }

    override fun initInject() {

    }

    override fun getLayoutId() = R.layout.fragment_detail_artist_album

    override fun getLogTag() = TAG

    override fun initView() {
        with(binding.recycler) {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(context, ConstantUtil.SPAN_COUNT_THREE)
        }
    }

    override fun initData() {
        mRecyclerAdapter = ArtistAlbumAdapter(
                WeakReference(activity as Activity),
                this::onAlbumClick,
                this::onAlbumLongClick
        )
        binding.recycler.adapter = mRecyclerAdapter
    }

    override fun onScrollToTop() {
        binding.recycler.scrollToPosition(0)
    }

    private fun addAlbumData(albumNameList: List<String>, allAlbumList: List<Album>) {
        run loop@{
            allAlbumList.forEach { album ->
                if (albumNameList.contains(album.albumName)) {
                    albumList.add(album)
                    albumCount += 1
                }
                if (albumCount == albumNameList.size) return@loop
            }
        }
    }

    companion object {

        private const val TAG = "DetailArtistAlbumFragment"
    }
}
