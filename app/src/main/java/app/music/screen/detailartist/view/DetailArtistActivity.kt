package app.music.screen.detailartist.view

import android.os.Bundle
import android.os.SystemClock
import android.view.Menu
import android.view.View
import androidx.lifecycle.Observer
import androidx.viewpager.widget.ViewPager
import app.music.R
import app.music.base.bottomprogress.BaseMVVMPBottomProgressActivity
import app.music.databinding.ActivityDetailArtistBinding
import app.music.model.entity.BaseMusik
import app.music.screen.detailartist.DetailArtistViewModel
import app.music.screen.detailplaymusic.view.DetailPlayMusicActivity
import app.music.utils.DoubleClickUtils
import app.music.utils.adapter.viewpager.DetailArtistPagerAdapter
import app.music.utils.blur.DynamicBlurUtils
import app.music.utils.listener.itemclick.AlbumFragmentItemClickListener
import com.google.android.material.bottomnavigation.BottomNavigationView

class DetailArtistActivity
    : BaseMVVMPBottomProgressActivity<ActivityDetailArtistBinding, DetailArtistViewModel>(),
        View.OnClickListener,
        AlbumFragmentItemClickListener,
        ViewPager.OnPageChangeListener {

    override val mRootViewId: Int = R.id.relative_root
    private var mNavigationItemLastReselectedTime: Long = 0
    lateinit var mDetailArtistTrackFragment: DetailArtistTrackFragment
    lateinit var mDetailArtistAlbumFragment: DetailArtistAlbumFragment

    private val mNavigationItemReselectedListener = BottomNavigationView.OnNavigationItemReselectedListener { menuItem ->
        if (DoubleClickUtils.isDoubleClick(mNavigationItemLastReselectedTime)) {
            when (menuItem.itemId) {
                R.id.navigation_song -> mDetailArtistTrackFragment.onScrollToTop()
                R.id.navigation_album -> mDetailArtistAlbumFragment.onScrollToTop()
            }
        }
        mNavigationItemLastReselectedTime = SystemClock.elapsedRealtime()
    }

    private val mNavigationItemSelectedListener =
            BottomNavigationView.OnNavigationItemSelectedListener { item ->
                with(mBinding.viewPager) {
                    when (item.itemId) {
                        R.id.navigation_song -> {
                            currentItem = 0
                            return@OnNavigationItemSelectedListener true
                        }
                        R.id.navigation_album -> {
                            currentItem = 1
                            return@OnNavigationItemSelectedListener true
                        }
                        else -> {
                        }
                    }
                }
                false
            }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(mViewModel) {
            observeLiveData(
                    mIsLaunchDetailPlayMusicActivity,
                    Observer { isLaunchDetailPlayMvActivity ->
                        isLaunchDetailPlayMvActivity.getValueIfNotHandled()?.let {
                            openActivity(DetailPlayMusicActivity::class.java, null)
                        }
                    }
            )
            observeLiveData(
                    mArtist,
                    Observer { artist ->
                        supportActionBar?.title = artist.artistName
                    }
            )
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_detail_artist
    }

    override fun initView() {
        with(mBinding) {
            mButtonPlayPause = btnPlayPause
            mTextPlayingArtist = txtPlayingArtist
            mTextPlayingSongName = txtPlayingSongName
            mImagePlayingCover = imgPlayingCover
            mBottomProgressBar = bottomProgressBar
            mButtonNext = btnNext
            mButtonPrev = btnPrev
            mBlurBottomBar = blurBottom
            super.initView()
            setupActionBar()
            with(mViewModel) {
                detailArtistViewModel = this
                with(this@DetailArtistActivity) {
                    DynamicBlurUtils.blurView(this, mRootViewId, blurBottomNavigation)
                    displayData(this, intent, coordinatorBackground, imgCoverArt)
                }
            }
            setupViewPager()
            setupBottomNavigation()
        }
    }

    override fun initInject() = activityComponent.inject(this)

    override fun getLogTag(): String {
        return TAG
    }

    override fun getOptionMenuId(): Int {
        return R.menu.activity_detail_artist
    }

    override fun createOptionMenu(menu: Menu) {

    }

    override fun initData() {

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {
        selectBottomNavigationItem(position)
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    fun getViewModel(): DetailArtistViewModel = mViewModel

    private fun setupViewPager() {
        with(mBinding.viewPager) {
            adapter = DetailArtistPagerAdapter(supportFragmentManager)
            addOnPageChangeListener(this@DetailArtistActivity)
        }
    }

    private fun setupBottomNavigation() {
        with(mBinding.bottomNavigation) {
            setOnNavigationItemSelectedListener(mNavigationItemSelectedListener)
            setOnNavigationItemReselectedListener(mNavigationItemReselectedListener)
            itemIconTintList = null
        }
        selectBottomNavigationItem(0)
    }

    private fun setupActionBar() {
        setSupportActionBar(mBinding.toolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
    }

    private fun selectBottomNavigationItem(position: Int) {
        with(mBinding.bottomNavigation.menu.getItem(position)) {
            isChecked = true
            mNavigationItemSelectedListener.onNavigationItemSelected(this)
        }
    }

    fun playPickedSong(musicList: MutableList<BaseMusik>, pickedSong: BaseMusik) {
        mViewModel.playPickedSong(musicList, pickedSong, binding.imgPlayingCover)
    }

    companion object {

        private const val TAG = "DetailArtistActivity"
    }
}
