package app.music.screen.detailartist.view


import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.os.SystemClock
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import app.music.R
import app.music.base.BaseFragment
import app.music.base.viewmodel.ContainListViewModel
import app.music.databinding.FragmentDetailArtistTrackBinding
import app.music.model.entity.Artist
import app.music.model.entity.BaseMusik
import app.music.screen.detailartist.DetailArtistViewModel
import app.music.utils.DoubleClickUtils
import app.music.utils.adapter.recycler.ArtistSongAdapter
import app.music.utils.comparator.comparatorascending.song.SongComparatorByAlphabetAscending
import app.music.utils.dialog.songoption.DialogSongOptionMethodUtils
import app.music.utils.listener.RecyclerScrollToTopListener
import app.music.utils.listener.dialoglistener.DialogSongOptionListener
import app.music.utils.recyclerview.RecyclerViewUtils
import app.music.utils.viewmodel.ViewModelUtils
import java.lang.ref.WeakReference
import java.util.*


/**
 * A simple [Fragment] subclass.
 */
class DetailArtistTrackFragment
    : BaseFragment<FragmentDetailArtistTrackBinding>(),
        RecyclerScrollToTopListener {

    private var mArtist: Artist? = null
    private var mRecyclerAdapter: ArtistSongAdapter? = null
    private lateinit var mDetailArtistViewModel: DetailArtistViewModel

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val activity = activity
        if (activity != null) {
            (activity as DetailArtistActivity).mDetailArtistTrackFragment = this
            mDetailArtistViewModel = activity.getViewModel()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mDetailArtistViewModel.mArtist.observe(
                this,
                Observer { artist ->
                    mArtist = artist
                    val musicList = mArtist!!.musicList
                    Collections.sort(musicList, SongComparatorByAlphabetAscending())
                    mRecyclerAdapter!!.updateItems(false, musicList)
                }
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.recycler.adapter = null
    }

    override fun initInject() {

    }

    override fun getLayoutId() = R.layout.fragment_detail_artist_track

    override fun getLogTag() = TAG

    override fun initView() {
        RecyclerViewUtils.setVerticalLinearLayout(context!!, binding.recycler, true)
        binding.recycler.itemAnimator = DefaultItemAnimator()
    }

    override fun initData() {
        mRecyclerAdapter = ArtistSongAdapter(
                WeakReference<Activity>(activity),
                this::onMusicClick,
                this::onMusicLongClick
        )
        binding.recycler.adapter = mRecyclerAdapter
    }

    override fun onScrollToTop() {
        binding.recycler.scrollToPosition(0)
    }

    private fun onMusicClick(music: BaseMusik) {
        checkDoubleClick {
            (activity as DetailArtistActivity).playPickedSong(mArtist?.musicList!!, music)
        }
    }

    private fun onMusicLongClick(music: BaseMusik) {
        DialogSongOptionMethodUtils.showSongOption(activity as DialogSongOptionListener, music)
    }

    private fun checkDoubleClick(listener: () -> Unit) {
        val mHomeActivityViewModel = ViewModelUtils.getViewModel<ContainListViewModel>(activity as Activity)
        with(mHomeActivityViewModel) {
            if (!DoubleClickUtils.isDoubleClick(getItemLastClickTime())) {
                setItemLastClickTime(SystemClock.elapsedRealtime())
                listener()
            } else {
                setItemLastClickTime(SystemClock.elapsedRealtime())
            }
        }
    }

    companion object {

        private const val TAG = "DetailArtistTrackFragment"
    }
}
