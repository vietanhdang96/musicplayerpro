package app.music.screen.setting

import android.view.Menu
import app.music.R
import app.music.base.mvvmp.BaseMVVMPActivity
import app.music.databinding.ActivitySettingBinding
import app.music.utils.blur.DynamicBlurUtils
import app.music.utils.intent.IntentMethodUtils
import kotlinx.android.synthetic.main.activity_setting.*


class SettingActivity
    : BaseMVVMPActivity<ActivitySettingBinding, SettingViewModel>() {

    override fun initInject() = activityComponent.inject(this)

    override fun getLayoutId() = R.layout.activity_setting

    override fun getLogTag() = TAG

    override fun getOptionMenuId() = 0

    override fun createOptionMenu(menu: Menu) {

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun initView() {
        with(binding) {
            DynamicBlurUtils.blurView(this@SettingActivity, R.id.coordinator_background, blurToolBar)
            setupActionBar()
            setViewClickListener()
            with(mViewModel) {
                settingViewModel = this
                checkDarkMode()
                mCompositeDisposable.add(
                        mIsDarkModeEnabled.addOnPropertyChanged {
                            changeThemeMode()
                            recreate()
                        })
            }
        }
    }

    override fun initData() {

    }

    private fun setupActionBar() {
        setSupportActionBar(tool_bar)
        supportActionBar?.run {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowTitleEnabled(false)
        }
    }

    private fun setViewClickListener() {
        text_theme.setOnClickListener { IntentMethodUtils.launchChooseThemeActivity(this) }
    }

    companion object {

        private const val TAG = "SettingActivity"
    }
}
