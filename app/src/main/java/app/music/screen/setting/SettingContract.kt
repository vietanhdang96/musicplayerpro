package app.music.screen.setting

import app.music.base.mvvmp.MVVMPContract


interface SettingContract {

    interface ViewModel : MVVMPContract.ViewModel {

        fun setDarkModeEnabledState(darkModeStatus: Boolean)
    }

    interface Presenter : MVVMPContract.Presenter {

        fun checkDarkMode()

        fun setDarkModeEnabled(isDarkModeEnabled: Boolean)
    }
}