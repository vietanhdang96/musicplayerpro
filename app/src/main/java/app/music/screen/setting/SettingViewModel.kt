package app.music.screen.setting

import androidx.databinding.ObservableBoolean
import app.music.base.mvvmp.BaseMVVMPViewModel
import javax.inject.Inject


class SettingViewModel @Inject constructor(
        settingPresenter: SettingPresenter)
    : BaseMVVMPViewModel<SettingPresenter>(settingPresenter),
        SettingContract.ViewModel {

    val mIsDarkModeEnabled = ObservableBoolean()

    override fun setDarkModeEnabledState(darkModeStatus: Boolean) {
        mIsDarkModeEnabled.set(darkModeStatus)
    }

    fun checkDarkMode() {
        mPresenter.checkDarkMode()
    }

    fun changeThemeMode() {
        mPresenter.setDarkModeEnabled(mIsDarkModeEnabled.get())
    }
}