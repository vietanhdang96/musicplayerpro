package app.music.screen.setting

import app.music.base.mvvmp.BaseMVVMPPresenter
import app.music.model.repository.setting.SettingRepository
import javax.inject.Inject

class SettingPresenter @Inject constructor(
        private val mSettingRepository: SettingRepository)
    : BaseMVVMPPresenter<SettingViewModel>(),
        SettingContract.Presenter {

    override fun checkDarkMode() {
        mViewModel?.setDarkModeEnabledState(mSettingRepository.getDarkModeEnabled())
    }

    override fun setDarkModeEnabled(isDarkModeEnabled: Boolean) {
        mSettingRepository.setDarkModeEnabled(isDarkModeEnabled)
    }
}
