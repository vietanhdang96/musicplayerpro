package app.music.screen.onlinehome

import app.music.R
import app.music.base.home.BaseHomeActivity
import app.music.databinding.ActivityOnlineHomeBinding
import app.music.utils.TabTitleUtils
import app.music.utils.adapter.viewpager.OnlineHomePagerAdapter
import app.music.utils.blur.DynamicBlurUtils
import app.music.utils.musicloading.LoadMusicUtil
import kotlinx.android.synthetic.main.activity_online_home.*
import kotlinx.android.synthetic.main.drawer_tile_offline_music.*
import retrofit2.Retrofit
import javax.inject.Inject

/**
 * Created by jacky on 3/23/18.
 */

class OnlineHomeActivity : BaseHomeActivity<OnlineHomeViewModel, ActivityOnlineHomeBinding>() {

    @Inject
    lateinit var mRetrofit: Retrofit
    override val rootViewId: Int = R.id.coordinator_background

    override fun initView() {
        super.initView()
        binding.onlineHomeViewModel = mHomeViewModel
        drawerTileOfflineMusic.setOnClickListener(this)
    }

    override fun initInject() = activityComponent.inject(this)

    override fun getLayoutId(): Int = R.layout.activity_online_home

    override fun getLogTag(): String = TAG

    override fun blurView() {
        super.blurView()
        DynamicBlurUtils.blurView(
                this@OnlineHomeActivity,
                R.id.relativeSideMenu,
                blurDrawerTitle
        )
    }

    override fun assignView() {
        with(mBinding) {
            mToolBar = toolBar
            mButtonPlayPause = btnPlayPause
            mButtonNext = btnNext
            mButtonPrev = btnPrev
            mSearchView = searchView
            mTextPlayingArtist = txtPlayingArtist
            mTextPlayingSongName = txtPlayingSongName
            mImagePlayingCover = imgPlayingCover
            mBackgroundImage = imageHomeBackground
            mBottomProgressBar = bottomProgressBar
            mBlurBottomBar = blurBottom
            mBottomNavigation = bottomNavigation
            mBlurBottomNavigation = blurBottomNavigation
            mDrawerLayout = drawerLayout
        }
        mViewPager = viewPager
        mBlurToolbar = blurToolBar
        mAppBarLayout = appBarHome
    }

    override fun editMenu() {
        with(mHomeViewModel) {
            setAlbumCount(LoadMusicUtil.sOnlineAlbumList.size)
            setSongCount(LoadMusicUtil.sOnlineMusicList.size)
            setArtistCount(LoadMusicUtil.sOnlineArtistList.size)
            setGenreCount(LoadMusicUtil.sOnlineGenreList.size)
            setPlaylistCount(LoadMusicUtil.sOnlinePlaylistList.size)
        }
    }

    override fun setupViewPager() {
        with(viewPager) {
            adapter = OnlineHomePagerAdapter(supportFragmentManager)
            offscreenPageLimit = TabTitleUtils.HOME_TAB_TITLE.size
        }
    }

    companion object {
        private const val TAG = "OnlineHomeActivity"
    }
}
