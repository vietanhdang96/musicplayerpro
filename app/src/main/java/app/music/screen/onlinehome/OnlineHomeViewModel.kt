package app.music.screen.onlinehome

import androidx.lifecycle.MutableLiveData
import app.music.base.home.BaseHomeViewModel
import app.music.model.entity.OnlineMusik
import app.music.utils.adapter.recycler.DialogOnlinePlaylistAdapter

class OnlineHomeViewModel : BaseHomeViewModel() {

    private var mLastAddToOnlinePlaylistObject = MutableLiveData<OnlineMusik>()
    private var mDialogOnlinePlaylistAdapter = MutableLiveData<DialogOnlinePlaylistAdapter>()

    fun setLastAddToOnlinePlaylistObject(music: OnlineMusik) {
        this.mLastAddToOnlinePlaylistObject.value = music
    }

    fun getLastAddToOnlinePlaylistObject(): OnlineMusik? {
        return mLastAddToOnlinePlaylistObject.value
    }

    fun setDialogOnlinePlaylistAdapter(adapter: DialogOnlinePlaylistAdapter) {
        this.mDialogOnlinePlaylistAdapter.value = adapter
    }

    fun getDialogOnlinePlaylistAdapter(): DialogOnlinePlaylistAdapter? {
        return mDialogOnlinePlaylistAdapter.value
    }
}
