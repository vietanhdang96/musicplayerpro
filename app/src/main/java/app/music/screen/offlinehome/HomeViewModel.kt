package app.music.screen.offlinehome

import androidx.databinding.ObservableInt
import androidx.lifecycle.MutableLiveData
import app.music.base.home.BaseHomeViewModel
import app.music.model.entity.Music
import app.music.utils.adapter.recycler.DialogPlaylistAdapter

class HomeViewModel : BaseHomeViewModel() {

    private var mLastAddToPlaylistObject = MutableLiveData<Music>()
    private var mDialogPlaylistAdapter = MutableLiveData<DialogPlaylistAdapter>()
    var mFolderRecyclerCurrentPosition = MutableLiveData(0)
    var mFolderCount = ObservableInt(0)

    fun setFolderCount(folderCount: Int) = mFolderCount.set(folderCount)

    fun setLastAddToPlaylistObject(music: Music) {
        this.mLastAddToPlaylistObject.value = music
    }

    fun getLastAddToPlaylistObject(): Music? = mLastAddToPlaylistObject.value

    fun setDialogPlaylistAdapter(adapter: DialogPlaylistAdapter) {
        this.mDialogPlaylistAdapter.value = adapter
    }

    fun setFolderRecyclerCurrentPosition(position: Int) {
        with(mFolderRecyclerCurrentPosition) {
            if (value != position) {
                value = position
            }
        }
    }

    fun getFolderRecyclerCurrentPosition(): Int {
        return mFolderRecyclerCurrentPosition.value ?: 0
    }

    fun getDialogPlaylistAdapter(): DialogPlaylistAdapter? = mDialogPlaylistAdapter.value
}
