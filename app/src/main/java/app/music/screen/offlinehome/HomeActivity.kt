package app.music.screen.offlinehome

import android.os.Bundle
import app.music.R
import app.music.base.home.BaseHomeActivity
import app.music.databinding.ActivityHomeBinding
import app.music.utils.TabTitleUtils
import app.music.utils.adapter.viewpager.HomePagerAdapter
import app.music.utils.blur.DynamicBlurUtils
import app.music.utils.listener.homefragmentlistener.FolderFragmentListener
import app.music.utils.musicloading.LoadMusicUtil
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.drawer_tile_folder.*
import kotlinx.android.synthetic.main.drawer_tile_online_music.*

/**
 * Created by jacky on 3/23/18.
 */

class HomeActivity : BaseHomeActivity<HomeViewModel, ActivityHomeBinding>() {

    private var mFolderFragmentListener: FolderFragmentListener? = null
    override val rootViewId: Int = R.id.coordinator_background

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeRecyclerCurrentPosition(mHomeViewModel.mFolderRecyclerCurrentPosition)
    }

    override fun initView() {
        super.initView()
        binding.homeViewModel = mHomeViewModel
        drawerTileOnlineMusic.setOnClickListener(this)
        drawerTileFolder.setOnClickListener(this)
    }

    override fun initInject() = activityComponent.inject(this)

    override fun getLayoutId(): Int = R.layout.activity_home

    override fun getLogTag(): String = TAG

    override fun blurView() {
        super.blurView()
        DynamicBlurUtils.blurView(
                this@HomeActivity,
                R.id.relativeSideMenu,
                blurDrawerTitle
        )
    }

    override fun assignView() {
        with(mBinding) {
            mToolBar = toolBar
            mButtonPlayPause = btnPlayPause
            mButtonNext = btnNext
            mButtonPrev = btnPrev
            mSearchView = searchView
            mTextPlayingArtist = txtPlayingArtist
            mTextPlayingSongName = txtPlayingSongName
            mImagePlayingCover = imgPlayingCover
            mBackgroundImage = imageHomeBackground
            mBottomProgressBar = bottomProgressBar
            mBlurBottomBar = blurBottom
            mBottomNavigation = bottomNavigation
            mBlurBottomNavigation = blurBottomNavigation
            mDrawerLayout = drawerLayout
        }
        mViewPager = viewPager
        mBlurToolbar = blurToolBar
        mAppBarLayout = appBarHome
    }

    override fun editMenu() {
        with(mHomeViewModel) {
            setAlbumCount(LoadMusicUtil.sAlbumList.size)
            setSongCount(LoadMusicUtil.sMusicList.size)
            setArtistCount(LoadMusicUtil.sArtistList.size)
            setGenreCount(LoadMusicUtil.sGenreList.size)
            setPlaylistCount(LoadMusicUtil.sPlaylistList.size)
            setFolderCount(LoadMusicUtil.sFolderList.size)
        }
    }

    fun setFolderFragmentListener(folderFragmentListener: FolderFragmentListener) {
        this.mFolderFragmentListener = folderFragmentListener
    }

    override fun setupViewPager() {
        with(viewPager) {
            adapter = HomePagerAdapter(supportFragmentManager)
            offscreenPageLimit = TabTitleUtils.HOME_TAB_TITLE.size
        }
    }

    companion object {
        private const val TAG = "HomeActivity"
    }
}
