package app.music.screen.splash

import android.app.Activity
import androidx.lifecycle.MutableLiveData
import androidx.work.OneTimeWorkRequest
import app.music.base.mvvmp.BaseMVVMPViewModel
import app.music.utils.livedata.HandleOnceLiveEvent
import app.music.utils.threadhandler.workmanager.LoadingMusicDoneWorker
import javax.inject.Inject

class SplashViewModel @Inject constructor(
        splashPresenter: SplashPresenter)
    : BaseMVVMPViewModel<SplashPresenter>(splashPresenter),
        SplashContract.ViewModel {

    override var mStoragePermissionGranted = false
    override var mLoadingMusicDoneWorkRequest = OneTimeWorkRequest
            .Builder(LoadingMusicDoneWorker::class.java)
            .build()
    var mLoadingThemeDataFinish = MutableLiveData<HandleOnceLiveEvent<Boolean>>()
    private var mLoadingMusicFinish = MutableLiveData(false)

    private fun setLoadingThemeDataFinish(isFinish: Boolean) {
        mLoadingThemeDataFinish.value = HandleOnceLiveEvent(isFinish)
    }

    fun getLoadingThemeDataFinish() = mLoadingThemeDataFinish.value?.getValue() ?: false

    fun setLoadingMusicFinish(isFinish: Boolean) {
        mLoadingMusicFinish.value = isFinish
    }

    fun getLoadingMusicFinish() = mLoadingMusicFinish.value ?: false

    override fun onCleared() {
        super.onCleared()
        mPresenter.removeHandlerCallBack()
    }

    override fun loadMusic() {
        mPresenter.loadMusic()
    }

    fun loadThemeData(activity: Activity) {
        mPresenter.loadThemeData(activity, ::setLoadingThemeDataFinish)
    }
}