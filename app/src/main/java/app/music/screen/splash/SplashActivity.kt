package app.music.screen.splash

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.Menu
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.work.WorkInfo
import androidx.work.WorkManager
import app.music.base.mvvmp.BaseMVVMPActivity
import app.music.databinding.ActivitySplashBinding
import app.music.utils.intent.IntentMethodUtils
import app.music.utils.musicloading.LoadMusicUtil
import app.music.utils.toast.ToastUtil
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener


/**
 * Created by jacky on 3/23/18.
 */

class SplashActivity
    : BaseMVVMPActivity<ActivitySplashBinding, SplashViewModel>() {

    private var isOnPausing = false
    private var mPermissionRequested = false
    private var isFinishedActivity = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadMusicDoneListener()
        with(mViewModel) {
            loadMusic()
            loadThemeData(this@SplashActivity)
        }
    }

    override fun onResume() {
        super.onResume()
        requestStoragePermission()
        isOnPausing = false
    }

    override fun onPause() {
        super.onPause()
        isOnPausing = true
    }

    override fun initInject() = activityComponent.inject(this)

    override fun getLayoutId() = 0

    override fun getLogTag() = TAG

    override fun getOptionMenuId() = 0

    override fun createOptionMenu(menu: Menu) {

    }

    override fun initView() {

    }

    override fun initData() {

    }

    private fun requestStoragePermission() {
        if (!mPermissionRequested) {
            mPermissionRequested = true
            Dexter.withActivity(this)
                    .withPermissions(
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.INTERNET,
                            Manifest.permission.CAMERA)
                    .withListener(object : MultiplePermissionsListener {
                        override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                            var permissionName: String
                            var message: String
                            for (deniedPermission in report.deniedPermissionResponses) {
                                when (deniedPermission.permissionName) {
                                    Manifest.permission.READ_EXTERNAL_STORAGE -> {
                                        permissionName = "STORAGE"
                                        message = "load offline music"
                                        showSettingsDialog(permissionName, message, true)
                                    }
                                    Manifest.permission.INTERNET -> {
                                        permissionName = "Internet"
                                        message = "load online music"
                                    }
                                    Manifest.permission.CAMERA -> {
                                        permissionName = "CAMERA"
                                        message = "take photo"
                                    }
                                }
                            }
                            for (grantedPermission in report.grantedPermissionResponses) {
                                when (grantedPermission.permissionName) {
                                    Manifest.permission.READ_EXTERNAL_STORAGE -> {
                                        mViewModel.mStoragePermissionGranted = true
                                    }
                                    Manifest.permission.INTERNET -> {
                                    }
                                    Manifest.permission.CAMERA -> {
                                    }
                                }
                            }
                        }

                        override fun onPermissionRationaleShouldBeShown(
                                permissions: MutableList<PermissionRequest>?,
                                token: PermissionToken?) {
                            token?.continuePermissionRequest()
                        }
                    }).withErrorListener { ToastUtil.showToast("Error occurred! ") }
                    .onSameThread()
                    .check()
        }
    }

    private fun loadMusicDoneListener() {
        LoadMusicUtil.sLoadOnlineMusicStatus = ""
        with(mViewModel) {
            WorkManager.getInstance()
                    .getWorkInfoByIdLiveData(mLoadingMusicDoneWorkRequest.id)
                    .observe(
                            this@SplashActivity as LifecycleOwner,
                            Observer<WorkInfo> { workInfo ->
                                if (workInfo != null
                                        && workInfo.state == WorkInfo.State.SUCCEEDED
                                        && getLoadingThemeDataFinish()) {
                                    setLoadingMusicFinish(true)
                                    navigateToHome()
                                }
                            })

            observeLiveData(
                    mLoadingThemeDataFinish,
                    Observer { mLoadingThemeDataFinish ->
                        mLoadingThemeDataFinish.getValueIfNotHandled()?.let {
                            if (getLoadingMusicFinish()) {
                                navigateToHome()
                            }
                        }
                    }
            )
        }
    }

    private fun navigateToHome() {
        if (!isFinishedActivity) {
            with(IntentMethodUtils) {
                with(ToastUtil) {
                    with(this@SplashActivity) {
                        when (LoadMusicUtil.sLoadOnlineMusicStatus) {
                            LoadMusicUtil.LOAD_ONLINE_MUSIC_SUCCEED -> {
                                if (LoadMusicUtil.sMusicList.size > 0) {
                                    if (!isOnPausing) {
                                        isFinishedActivity = true
                                        launchHomeActivity(this, true)
                                    }
                                } else {
                                    showToast("there's no song in your device")
                                    exitApplication()
                                }
                            }

                            LoadMusicUtil.LOAD_ONLINE_MUSIC_FAILED -> {
                                if (LoadMusicUtil.sMusicList.size > 0) {
                                    if (!isOnPausing) {
                                        isFinishedActivity = true
                                        showToast("online music load failed")
                                        launchHomeActivity(this, true)
                                    }
                                } else {
                                    showToast("there's no song in your device")
                                    exitApplication()
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun showSettingsDialog(permissionName: String, message: String,
                                   isMustBeEnabledPermission: Boolean) {
        AlertDialog.Builder(this).run {
            setTitle("Need Permissions")
            setMessage(
                    (if (isMustBeEnabledPermission) "$permissionName permission must be enabled "
                    else "This app needs $permissionName permission ")
                            + "to $message. You can grant permission in APP SETTINGS -> PERMISSION.")
            setPositiveButton("GOTO APP SETTINGS") { dialog, which ->
                dialog.cancel()
                openSettings(packageName)
                mPermissionRequested = false
            }
            if (!isMustBeEnabledPermission) {
                setNegativeButton("Cancel") { dialog, which ->
                    dialog.cancel()
                }
            }
            show()
        }
    }

    private fun openSettings(packageName: String) {
        startActivity(
                Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply {
                    addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    data = Uri.fromParts("package", packageName, null)
                }
        )
    }

    private fun exitApplication() {
        if (Build.VERSION.SDK_INT in 16..20) {
            finishAffinity()
        } else if (Build.VERSION.SDK_INT >= 21) {
            finishAndRemoveTask()
        }
    }

    companion object {

        private const val TAG = "SplashActivity"
    }
}
