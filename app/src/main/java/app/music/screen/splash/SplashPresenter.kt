package app.music.screen.splash

import android.app.Activity
import android.os.Handler
import app.music.base.mvvmp.BaseMVVMPPresenter
import app.music.model.repository.music.MusicRepository
import app.music.model.repository.theme.ThemeRepository
import javax.inject.Inject

class SplashPresenter @Inject constructor(
        private val mMusicRepository: MusicRepository,
        private val mThemeRepository: ThemeRepository,
        private val mHandler: Handler)
    : BaseMVVMPPresenter<SplashViewModel>(),
        SplashContract.Presenter {

    private var mMusicLoadingStarted = false

    override fun loadMusic() {
        mHandler.post(object : Runnable {
            override fun run() {
                mViewModel?.run {
                    if (mStoragePermissionGranted && !mMusicLoadingStarted) {
                        mMusicLoadingStarted = true
                        mMusicRepository.getAllMusicList(mLoadingMusicDoneWorkRequest)
                        mHandler.removeCallbacksAndMessages(null)
                    }
                }
                mHandler.postDelayed(this, 100)
            }
        })
    }

    override fun removeHandlerCallBack() {
        mHandler.removeCallbacksAndMessages(null)
    }

    fun loadThemeData(activity: Activity, loadThemeDataFinish: (Boolean) -> Unit) {
        mThemeRepository.loadThemeData(activity, loadThemeDataFinish)
    }
}
