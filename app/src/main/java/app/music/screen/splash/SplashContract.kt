package app.music.screen.splash

import androidx.work.OneTimeWorkRequest
import app.music.base.mvvmp.MVVMPContract

interface SplashContract {

    interface ViewModel : MVVMPContract.ViewModel {

        var mStoragePermissionGranted: Boolean
        var mLoadingMusicDoneWorkRequest: OneTimeWorkRequest

        fun loadMusic()
    }

    interface Presenter : MVVMPContract.Presenter {

        fun loadMusic()

        fun removeHandlerCallBack()
    }
}