package app.music.base.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class ContainListViewModel : ViewModel() {

    private var mItemLastClickTime = MutableLiveData<Long>()

    fun setItemLastClickTime(itemLastClickTime: Long) {
        this.mItemLastClickTime.value = itemLastClickTime
    }

    fun getItemLastClickTime(): Long {
        return mItemLastClickTime.value?.toLong() ?: 0
    }
}