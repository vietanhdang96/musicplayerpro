package app.music.base

import androidx.recyclerview.widget.DiffUtil

abstract class BaseDiffCallBack<T>(
        protected val mOldList: List<T>,
        protected val mNewList: List<T>)
    : DiffUtil.Callback() {

    override fun getOldListSize(): Int = mOldList.size

    override fun getNewListSize(): Int = mNewList.size
}
