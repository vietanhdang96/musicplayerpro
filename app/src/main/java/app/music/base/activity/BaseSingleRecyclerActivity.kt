package app.music.base.activity

import android.content.Intent
import android.os.SystemClock
import android.text.TextUtils
import app.music.R
import app.music.base.bottomprogress.BaseBottomProgressActivity
import app.music.databinding.ActivityDetailAlbumBinding
import app.music.model.entity.BaseMusik
import app.music.model.entity.OnlineMusik
import app.music.utils.ConstantUtil
import app.music.utils.DoubleClickUtils
import app.music.utils.adapter.recycler.AlbumSongAdapter
import app.music.utils.gradient.GradientBackgroundUtils
import app.music.utils.imageloading.ImageLoadingUtils
import app.music.utils.recyclerview.RecyclerViewUtils
import app.music.utils.toast.ToastUtil
import com.bumptech.glide.request.RequestOptions
import java.lang.ref.WeakReference
import java.util.*

abstract class BaseSingleRecyclerActivity<ObjectType>
    : BaseBottomProgressActivity<ActivityDetailAlbumBinding>() {

    private var mMusicList: List<BaseMusik> = ArrayList()
    private var mLastClickTime: Long = 0
    private val mRequestOptions = RequestOptions().error(R.drawable.ic_album)
    private lateinit var mRecyclerAdapter: AlbumSongAdapter
    override val rootViewId = R.id.relative_detail_album

    override fun onDestroy() {
        super.onDestroy()
        mBinding.recyclerView.adapter = null
    }

    override fun getLayoutId() = R.layout.activity_detail_album

    override fun initView() {
        with(mBinding) {
            mButtonPlayPause = btnPlayPause
            mTextPlayingArtist = txtPlayingArtist
            mTextPlayingSongName = txtPlayingSongName
            mImagePlayingCover = imgPlayingCover
            mBottomProgressBar = bottomProgressBar
            mButtonNext = btnNext
            mButtonPrev = btnPrev
            mBlurBottomBar = blurBottom
            super.initView()

            val intent = intent
            var dataObject: ObjectType? = null
            if (intent != null) {
                dataObject = getDataObject(intent)
            }
            setupActionBar(dataObject)
            mMusicList = ArrayList(getDataList(dataObject))
            with(this@BaseSingleRecyclerActivity) {
                if (mMusicList.isNotEmpty()) {
                    val music = mMusicList[0]
                    val musicType = music.type
                    if (!TextUtils.isEmpty(musicType)) {
                        when (musicType) {
                            ConstantUtil.OFFLINE_MUSIC -> {
                                val songLocation = music.location
                                if (!TextUtils.isEmpty(songLocation)) {
                                    with(mMetadataRetriever) {
                                        setDataSource(songLocation)
                                        val bytes = embeddedPicture
                                        if (bytes != null) {
                                            GradientBackgroundUtils.createGradientBackground(
                                                    this@BaseSingleRecyclerActivity,
                                                    bytes,
                                                    coordinatorBackground
                                            )
                                        }
                                        ImageLoadingUtils.loadImage(imgCoverArt, bytes, mRequestOptions)
                                    }
                                }
                            }
                            ConstantUtil.ONLINE_MUSIC -> {
                                val coverArtLink = (music as OnlineMusik).coverArt
                                if (!TextUtils.isEmpty(coverArtLink)) {
                                    GradientBackgroundUtils.createGradientBackground(
                                            this,
                                            coverArtLink,
                                            coordinatorBackground
                                    )
                                }
                                ImageLoadingUtils.loadImage(imgCoverArt, coverArtLink, mRequestOptions)
                            }
                            else -> {
                            }
                        }
                    }
                }
                RecyclerViewUtils.setVerticalLinearLayout(this, recyclerView, true, true)
            }
        }
    }

    override fun initData() {
        mRecyclerAdapter = AlbumSongAdapter(
                WeakReference(this),
                this::onSongClick,
                this::onSongLongClick
        )
        mBinding.recyclerView.adapter = mRecyclerAdapter
        mRecyclerAdapter.updateItems(false, mMusicList)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun setupActionBar(dataObject: ObjectType?) {
        setSupportActionBar(mBinding.toolbar)
        val toolbarTitle = getToolbarTitle(dataObject)
        supportActionBar?.run {
            setDisplayHomeAsUpEnabled(true)
            if (!TextUtils.isEmpty(toolbarTitle)) {
                title = toolbarTitle
            }
        }
    }

    private fun onSongClick(musik: BaseMusik) {
        if (DoubleClickUtils.isDoubleClick(mLastClickTime)) return
        mLastClickTime = SystemClock.elapsedRealtime()
        mMusicService.setList(mMusicList)
        playPickedSong(musik)
    }

    private fun onSongLongClick(musik: BaseMusik) {
        if (DoubleClickUtils.isDoubleClick(mLastClickTime)) return
        mLastClickTime = SystemClock.elapsedRealtime()
        ToastUtil.showToast("Song Long click${musik.title}")
    }

    protected abstract fun getDataObject(intent: Intent): ObjectType?

    protected abstract fun getToolbarTitle(dataObject: ObjectType?): String?

    protected abstract fun getDataList(dataObject: ObjectType?): List<BaseMusik>
}
