package app.music.base.bottomprogress

import android.app.Activity
import android.content.Context
import android.widget.ImageView
import app.music.R
import app.music.base.musicservice.BaseMVVMPMusicPresenter
import app.music.model.entity.BaseMusik
import app.music.model.entity.OnlineMusik
import app.music.model.repository.music.MusicRepository
import app.music.utils.ConstantUtil
import app.music.utils.blur.DynamicBlurUtils
import app.music.utils.imageloading.ImageLoadingUtils
import app.music.utils.musicloading.LoadMusicUtil
import com.bumptech.glide.request.RequestOptions
import eightbitlab.com.blurview.BlurView
import java.util.*

abstract class BaseMVVMPBottomProgressPresenter<VM : MVVMPBottomProgressContract.ViewModel>
constructor(protected val mMusicRepository: MusicRepository)
    : BaseMVVMPMusicPresenter<VM>(),
        MVVMPBottomProgressContract.Presenter {

    private val mRequestOptions = RequestOptions().error(R.drawable.ic_bottom_bar_disc)

    override fun setPlayButtonImageResource() {
        mViewModel?.setPlaying(mMusicService?.exoPlayer?.playWhenReady!!)
    }

    override fun displayPlayingSongInfo(imageCoverArt: ImageView) {
        displayPlayingSongInfo(
                ::pausePlayer,
                { loadLastState(imageCoverArt) },
                { updateSongInfo(imageCoverArt) },
                ::setPlayButtonImageResource,
                ::setSeekBarProgress
        )
    }

    private fun setSeekBarProgress(progress: Int) {
        mViewModel?.setSeekBarProgress(progress)
    }

    override fun loadCoverArt(link: String, imageCoverArt: ImageView?) {
        with(mMetadataRetriever) {
            setDataSource(link)
            ImageLoadingUtils.loadImage(imageCoverArt!!, embeddedPicture, mRequestOptions)
        }
    }

    override fun loadOnlineCoverArt(link: String, imageCoverArt: ImageView?) {
        ImageLoadingUtils.loadImage(imageCoverArt!!, link, mRequestOptions)
    }

    override fun changeExoPlayerState(context: Context) {
        changeExoPlayerState(
                context,
                { pausePlayer() },
                {
                    mMusicService?.run {
                        addPlayerListener()
                        startService(context)
                        playPlayer()
                    }
                },
                { mViewModel?.getSeekBarProgress()?.toLong()!! },
                null
        )
    }

    override fun blurView(activity: Activity, rootViewId: Int, blurBottomBar: BlurView) {
        DynamicBlurUtils.blurView(activity, rootViewId, blurBottomBar)
    }

    override fun setLastState(music: BaseMusik, imageCoverArt: ImageView) {
        with(music) {
            when (type) {
                ConstantUtil.OFFLINE_MUSIC -> loadCoverArt(location, imageCoverArt)
                ConstantUtil.ONLINE_MUSIC -> {
                    val coverArtLink = (this as OnlineMusik).coverArt
                    loadOnlineCoverArt(coverArtLink, imageCoverArt)
                }
            }
            mViewModel?.run {
                setSongTitle(title)
                setArtistName(artist)
                setSeekBarMax(duration)
                setSeekBarProgress(mMusicRepository.getLastCountTime().toInt())
                setPlaying(false)
            }
        }
    }

    override fun playPickedSong(music: BaseMusik, imageCoverArt: ImageView) {
        mMusicService?.run {
            if (mMusicBound) {
                setSong(music)
                mPlayIntent.action = ConstantUtil.ACTION.STARTFOREGROUND_ACTION
                startService(mPlayIntent)
                playSong()
                updateSongInfo(imageCoverArt)
                setPlayerShuffleMode()
                setPlayerRepeatMode()
//                openActivity(DetailPlayMusicActivity::class.java, null)
                mViewModel?.setLaunchDetailPlayMusicActivity(true)
            }
        }
    }

    override fun updateSongInfo(imageCoverArt: ImageView) {
        mMusicService?.run {
            if (songs.size > 0) {
                with(playingSong) {
                    when (type) {
                        ConstantUtil.OFFLINE_MUSIC -> loadCoverArt(location, imageCoverArt)
                        ConstantUtil.ONLINE_MUSIC -> {
                            loadOnlineCoverArt((this as OnlineMusik).coverArt, imageCoverArt)
                        }
                    }
                    mViewModel?.run {
                        setSongTitle(title)
                        setArtistName(artist)
                        setSeekBarMax(duration)
                    }
                }
            }
        }
        setPlayButtonImageResource()
    }

    override fun pausePlayer() {
        mViewModel?.setPlaying(false)
        mMusicService?.pausePlayer()
    }

    override fun loadLastState(imageCoverArt: ImageView) {
        mLastMusicList = ArrayList(mMusicRepository.getLastPlayedMusicList())
        if (mLastMusicList.isNotEmpty()) {
            mLastPlayedMusicObject = mMusicRepository.getLastPlayedSong()
            if (mLastPlayedMusicObject == null) {
                mLastPlayedMusicObject = LoadMusicUtil.sMusicList[0]
            }
        } else {
            mLastMusicList = LoadMusicUtil.sMusicList
            mLastPlayedMusicObject = LoadMusicUtil.sMusicList[0]
        }

        val music = mLastPlayedMusicObject
        if (music != null) {
            setLastState(music, imageCoverArt)
        }
    }
}
