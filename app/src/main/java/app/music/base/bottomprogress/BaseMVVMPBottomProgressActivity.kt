package app.music.base.bottomprogress

import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.databinding.ViewDataBinding
import app.music.R
import app.music.base.musicservice.BaseMVVMPMusicActivity
import app.music.screen.detailplaymusic.view.DetailPlayMusicActivity
import de.hdodenhof.circleimageview.CircleImageView
import eightbitlab.com.blurview.BlurView

abstract class BaseMVVMPBottomProgressActivity<
        B : ViewDataBinding,
        VM : MVVMPBottomProgressContract.ViewModel>
    : BaseMVVMPMusicActivity<B, VM>(),
        View.OnClickListener {

    protected var mButtonPlayPause: ImageButton? = null
    protected var mTextPlayingArtist: TextView? = null
    protected var mTextPlayingSongName: TextView? = null
    protected var mImagePlayingCover: CircleImageView? = null
    protected var mBottomProgressBar: ProgressBar? = null
    protected var mButtonNext: ImageButton? = null
    protected var mButtonPrev: ImageButton? = null
    protected lateinit var mBlurBottomBar: BlurView
    private val mLogTag = "BaseBottomProgressActivity"
    protected abstract val mRootViewId: Int

    override fun onStart() {
        super.onStart()
        mViewModel.displayPlayingSongInfo(mImagePlayingCover as ImageView)
    }

    override fun onClick(v: View) {
        with(mViewModel) {
            with(this@BaseMVVMPBottomProgressActivity) {
                when (v.id) {
                    R.id.btn_next -> playNextSong(this)
                    R.id.btn_prev -> playPreviousSong(this)
                    R.id.btn_play_pause -> changeExoPlayerState(this)
                    R.id.blurBottom -> openActivity(DetailPlayMusicActivity::class.java, null)
                }
            }
        }
    }

    override fun initView() {
        bindMusicService()
        assignViews(mButtonNext, mButtonPrev, mButtonPlayPause, mBlurBottomBar)
        mViewModel.blurView(this, mRootViewId, mBlurBottomBar)
    }

    private fun assignViews(vararg views: View?) {
        views.forEach { it?.setOnClickListener(this) }
    }
}
