package app.music.base.bottomprogress

import android.app.Activity
import android.content.Context
import android.widget.ImageView
import app.music.base.musicservice.MVVMPMusicContract
import app.music.model.entity.BaseMusik
import eightbitlab.com.blurview.BlurView

interface MVVMPBottomProgressContract {

    interface ViewModel : MVVMPMusicContract.ViewModel {

        fun displayPlayingSongInfo(imageCoverArt: ImageView)

        fun playNextSong(context: Context)

        fun playPreviousSong(context: Context)

        fun changeExoPlayerState(context: Context)

        fun blurView(activity: Activity, rootViewId: Int, blurBottomBar: BlurView)

        fun setLaunchDetailPlayMusicActivity(launchDetailPlayMvActivity: Boolean)
    }

    interface Presenter : MVVMPMusicContract.Presenter {

        fun setPlayButtonImageResource()

        fun loadCoverArt(link: String, imageCoverArt: ImageView?)

        fun loadOnlineCoverArt(link: String, imageCoverArt: ImageView?)

        fun setLastState(music: BaseMusik, imageCoverArt: ImageView)

        fun playPickedSong(music: BaseMusik, imageCoverArt: ImageView)

        fun updateSongInfo(imageCoverArt: ImageView)

        fun pausePlayer()

        fun loadLastState(imageCoverArt: ImageView)

        fun displayPlayingSongInfo(imageCoverArt: ImageView)

        fun changeExoPlayerState(context: Context)

        fun blurView(activity: Activity, rootViewId: Int, blurBottomBar: BlurView)
    }
}