package app.music.base.bottomprogress

import android.app.Activity
import android.content.Context
import android.widget.ImageView
import androidx.lifecycle.MutableLiveData
import app.music.base.musicservice.BaseMVVMPMusicViewModel
import app.music.utils.livedata.HandleOnceLiveEvent
import eightbitlab.com.blurview.BlurView

abstract class BaseMVVMPBottomProgressViewModel<P : MVVMPBottomProgressContract.Presenter>
constructor(presenter: P)
    : BaseMVVMPMusicViewModel<P>(presenter),
        MVVMPBottomProgressContract.ViewModel {

    var mIsLaunchDetailPlayMusicActivity = MutableLiveData<HandleOnceLiveEvent<Boolean>>()

    override fun setLaunchDetailPlayMusicActivity(launchDetailPlayMvActivity: Boolean) {
        mIsLaunchDetailPlayMusicActivity.value = HandleOnceLiveEvent(launchDetailPlayMvActivity)
    }

    override fun displayPlayingSongInfo(imageCoverArt: ImageView) {
        mPresenter.displayPlayingSongInfo(imageCoverArt)
    }

    override fun playPreviousSong(context: Context) {
        mPresenter.playPreviousSong(context)
    }

    override fun playNextSong(context: Context) {
        mPresenter.playNextSong(context)
    }

    override fun changeExoPlayerState(context: Context) {
        mPresenter.changeExoPlayerState(context)
    }

    override fun blurView(activity: Activity, rootViewId: Int, blurBottomBar: BlurView) {
        mPresenter.blurView(activity, rootViewId, blurBottomBar)
    }
}
