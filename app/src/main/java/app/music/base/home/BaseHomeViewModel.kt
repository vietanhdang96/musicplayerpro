package app.music.base.home

import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.MutableLiveData
import app.music.base.viewmodel.ContainListViewModel
import app.music.utils.DrawerConstantUtils
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.rengwuxian.materialedittext.MaterialEditText

open class BaseHomeViewModel : ContainListViewModel() {

    var mIsSearching = MutableLiveData<Boolean>()
    private var mSearchingText = MutableLiveData<String>()
    private var mPlaylistDialog = MutableLiveData<BottomSheetDialog>()
    private var mNewPlaylistDialog = MutableLiveData<BottomSheetDialog>()
    private var mEditNewPlaylistName = MutableLiveData<MaterialEditText>()
    var mAlbumRecyclerCurrentPosition = MutableLiveData(0)
    var mSongRecyclerCurrentPosition = MutableLiveData(0)
    var mArtistRecyclerCurrentPosition = MutableLiveData(0)
    var mGenreRecyclerCurrentPosition = MutableLiveData(0)
    var mPlaylistRecyclerCurrentPosition = MutableLiveData(0)
    var mAlbumCount = ObservableInt(0)
    var mSongCount = ObservableInt(0)
    var mArtistCount = ObservableInt(0)
    var mGenreCount = ObservableInt(0)
    var mPlaylistCount = ObservableInt(0)
    var mSelectedDrawerTile = ObservableField<String>(DrawerConstantUtils.DRAWER_TILE_SONG)

    fun setSelectedDrawerTile(selectedDrawerTile: String?) {
        mSelectedDrawerTile.set(selectedDrawerTile)
    }

    fun getSelectedDrawerTile() = mSelectedDrawerTile.get()

    fun setAlbumCount(albumCount: Int) {
        mAlbumCount.set(albumCount)
    }

    fun setSongCount(songCount: Int) {
        mSongCount.set(songCount)
    }

    fun setArtistCount(artistCount: Int) {
        mArtistCount.set(artistCount)
    }

    fun setGenreCount(genreCount: Int) {
        mGenreCount.set(genreCount)
    }

    fun setPlaylistCount(playlistCount: Int) {
        mPlaylistCount.set(playlistCount)
    }

    fun setAlbumRecyclerCurrentPosition(position: Int) {
        with(mAlbumRecyclerCurrentPosition) {
            if (value != position) {
                value = position
            }
        }
    }

    fun getAlbumRecyclerCurrentPosition(): Int {
        return mAlbumRecyclerCurrentPosition.value ?: 0
    }

    fun setSongRecyclerCurrentPosition(position: Int) {
        with(mSongRecyclerCurrentPosition) {
            if (value != position) {
                value = position
            }
        }
    }

    fun getSongRecyclerCurrentPosition(): Int {
        return mSongRecyclerCurrentPosition.value ?: 0
    }

    fun setArtistRecyclerCurrentPosition(position: Int) {
        with(mArtistRecyclerCurrentPosition) {
            if (value != position) {
                value = position
            }
        }
    }

    fun getArtistRecyclerCurrentPosition(): Int {
        return mArtistRecyclerCurrentPosition.value ?: 0
    }

    fun setGenreRecyclerCurrentPosition(position: Int) {
        with(mGenreRecyclerCurrentPosition) {
            if (value != position) {
                value = position
            }
        }
    }

    fun getGenreRecyclerCurrentPosition(): Int {
        return mGenreRecyclerCurrentPosition.value ?: 0
    }

    fun setPlaylistRecyclerCurrentPosition(position: Int) {
        with(mPlaylistRecyclerCurrentPosition) {
            if (value != position) {
                value = position
            }
        }
    }

    fun getPlaylistRecyclerCurrentPosition(): Int {
        return mPlaylistRecyclerCurrentPosition.value ?: 0
    }

    fun setPlaylistDialog(playlistDialog: BottomSheetDialog) {
        this.mPlaylistDialog.value = playlistDialog
    }

    fun getPlaylistDialog(): BottomSheetDialog? {
        return mPlaylistDialog.value
    }

    fun setNewPlaylistDialog(newPlaylistDialog: BottomSheetDialog) {
        this.mNewPlaylistDialog.value = newPlaylistDialog
    }

    fun getNewPlaylistDialog(): BottomSheetDialog? {
        return mNewPlaylistDialog.value
    }

    fun setEditNewPlaylistName(newPlaylistDialog: MaterialEditText) {
        this.mEditNewPlaylistName.value = newPlaylistDialog
    }

    fun getEditNewPlaylistName(): MaterialEditText? {
        return mEditNewPlaylistName.value
    }

    fun setSearchingText(searchingText: String?) {
        this.mSearchingText.value = searchingText
    }

    fun getSearchingText(): String? {
        return mSearchingText.value?.toString() ?: null
    }

    fun setSearching(searching: Boolean) {
        this.mIsSearching.value = searching
    }

    fun getSearching(): Boolean {
        return mIsSearching.value ?: false
    }
}
