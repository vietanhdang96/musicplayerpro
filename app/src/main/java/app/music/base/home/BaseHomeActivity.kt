package app.music.base.home

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.speech.RecognizerIntent
import android.text.TextUtils
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.databinding.ViewDataBinding
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.viewpager.widget.ViewPager
import app.music.R
import app.music.base.bottomprogress.BaseBottomProgressActivity
import app.music.model.entity.BaseMusik
import app.music.model.entity.OnlineMusik
import app.music.screen.setting.SettingActivity
import app.music.utils.ConstantUtil
import app.music.utils.DoubleClickUtils
import app.music.utils.DrawerConstantUtils
import app.music.utils.blur.BlurImageUtils
import app.music.utils.blur.DynamicBlurUtils
import app.music.utils.imageloading.ImageLoadingUtils
import app.music.utils.intent.IntentMethodUtils
import app.music.utils.listener.ToolbarScrollFlagListener
import app.music.utils.listener.dialoglistener.DialogAddToPlaylistListener
import app.music.utils.listener.dialoglistener.DialogHomeItemSortListener
import app.music.utils.listener.dialoglistener.DialogSongOptionListener
import app.music.utils.listener.homefragmentlistener.*
import app.music.utils.log.InformationLogUtils
import br.com.mauker.materialsearchview.MaterialSearchView
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomsheet.BottomSheetDialog
import eightbitlab.com.blurview.BlurView
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.drawer_tile_album.*
import kotlinx.android.synthetic.main.drawer_tile_artist.*
import kotlinx.android.synthetic.main.drawer_tile_equalizer.*
import kotlinx.android.synthetic.main.drawer_tile_favorite.*
import kotlinx.android.synthetic.main.drawer_tile_feedback.*
import kotlinx.android.synthetic.main.drawer_tile_genre.*
import kotlinx.android.synthetic.main.drawer_tile_playlist.*
import kotlinx.android.synthetic.main.drawer_tile_settings.*
import kotlinx.android.synthetic.main.drawer_tile_share.*
import kotlinx.android.synthetic.main.drawer_tile_sleep_timer.*
import kotlinx.android.synthetic.main.drawer_tile_song.*
import kotlinx.android.synthetic.main.drawer_tile_we_are_social.*
import java.util.*
import javax.inject.Inject

/**
 * Created by jacky on 3/23/18.
 */

abstract class BaseHomeActivity<VM : BaseHomeViewModel, B : ViewDataBinding>
    : BaseBottomProgressActivity<B>(),
        ViewPager.OnPageChangeListener,
        DialogAddToPlaylistListener,
        DialogSongOptionListener,
        DialogHomeItemSortListener,
        View.OnClickListener,
        ToolbarScrollFlagListener<VM> {

    @Inject
    override lateinit var mHomeViewModel: VM
    override var mAppBarLayout: AppBarLayout? = null
    override var mSongOptionDialog: BottomSheetDialog? = null
    override var mAlbumSortDialog: BottomSheetDialog? = null
    override var mSongSortDialog: BottomSheetDialog? = null
    override var mArtistSortDialog: BottomSheetDialog? = null
    override var mGenreSortDialog: BottomSheetDialog? = null
    override var mPlaylistSortDialog: BottomSheetDialog? = null
    override var mMenuSortDialog: BottomSheetDialog? = null
    abstract override val rootViewId: Int
    override var mAlbumFragmentListener: AlbumFragmentListener? = null
    override var mSongFragmentListener: SongFragmentListener? = null
    override var mArtistFragmentListener: ArtistFragmentListener? = null
    override var mGenreFragmentListener: GenreFragmentListener? = null
    override var mPlaylistFragmentListener: PlaylistFragmentListener? = null
    private val mRequestOptions = RequestOptions().error(R.drawable.bg_main_color)
    private var mNavigationItemLastReselectedTime: Long = 0
    protected lateinit var mToolBar: Toolbar
    override lateinit var mBlurToolbar: BlurView
    protected lateinit var mBottomNavigation: BottomNavigationView
    protected lateinit var mBlurBottomNavigation: BlurView
    protected lateinit var mSearchView: MaterialSearchView
    protected lateinit var mViewPager: ViewPager
    protected lateinit var mDrawerLayout: DrawerLayout
    protected lateinit var mBackgroundImage: ImageView
    private var lastSelectedHomeFragment: String? = null

    private val mNavigationItemReselectedListener =
            BottomNavigationView.OnNavigationItemReselectedListener { menuItem ->
                if (DoubleClickUtils.isDoubleClick(mNavigationItemLastReselectedTime)) {
                    (when (menuItem.itemId) {
                        R.id.navigation_album -> {
                            mAlbumFragmentListener!!
                        }
                        R.id.navigation_song -> {
                            mSongFragmentListener!!
                        }
                        R.id.navigation_artist -> {
                            mArtistFragmentListener!!
                        }
                        R.id.navigation_genre -> {
                            mGenreFragmentListener!!
                        }
                        R.id.navigation_playlist -> {
                            mPlaylistFragmentListener!!
                        }
                        else -> {
                            null
                        }
                    } as BaseHomeFragment<*, *, *, *, *>).onScrollToTop()
                }
                mNavigationItemLastReselectedTime = SystemClock.elapsedRealtime()
            }

    private val mNavigationItemSelectedListener =
            BottomNavigationView.OnNavigationItemSelectedListener { item ->
                with(mHomeViewModel) {
                    with(mViewPager) {
                        when (item.itemId) {
                            R.id.navigation_album -> {
                                // todo fix this trigger twice when swipe to third tab, and click album bottom navigation tab
                                setToolBarScrollFlag(getAlbumRecyclerCurrentPosition())
                                currentItem = 0
                                return@OnNavigationItemSelectedListener true
                            }
                            R.id.navigation_song -> {
                                setToolBarScrollFlag(getSongRecyclerCurrentPosition())
                                currentItem = 1
                                return@OnNavigationItemSelectedListener true
                            }
                            R.id.navigation_artist -> {
                                setToolBarScrollFlag(getArtistRecyclerCurrentPosition())
                                currentItem = 2
                                return@OnNavigationItemSelectedListener true
                            }
                            R.id.navigation_genre -> {
                                setToolBarScrollFlag(getGenreRecyclerCurrentPosition())
                                currentItem = 3
                                return@OnNavigationItemSelectedListener true
                            }
                            R.id.navigation_playlist -> {
                                setToolBarScrollFlag(getPlaylistRecyclerCurrentPosition())
                                currentItem = 4
                                return@OnNavigationItemSelectedListener true
                            }
                        }
                        false
                    }
                }
            }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(mHomeViewModel) {
            observeRecyclerCurrentPosition(
                    mAlbumRecyclerCurrentPosition,
                    mSongRecyclerCurrentPosition,
                    mArtistRecyclerCurrentPosition,
                    mGenreRecyclerCurrentPosition,
                    mPlaylistRecyclerCurrentPosition
            )
        }
    }

    override fun onResume() {
        super.onResume()
        mSearchView.activityResumed()
    }

    override fun onStop() {
        super.onStop()
        lastSelectedHomeFragment?.let {
            setSelectedDrawerTile(lastSelectedHomeFragment)
            lastSelectedHomeFragment = null
        }
    }

    override fun onClick(v: View) {
        with(DrawerConstantUtils) {
            with(IntentMethodUtils) {
                with(this@BaseHomeActivity) {
                    when (v.id) {
                        R.id.drawerTileAlbum -> {
                            viewPager.currentItem = 0
                            changeSelectedDrawerTile(DRAWER_TILE_ALBUM)
                        }
                        R.id.drawerTileSong -> {
                            viewPager.currentItem = 1
                            changeSelectedDrawerTile(DRAWER_TILE_SONG)
                        }
                        R.id.drawerTileArtist -> {
                            viewPager.currentItem = 2
                            changeSelectedDrawerTile(DRAWER_TILE_ARTIST)
                        }
                        R.id.drawerTileGenre -> {
                            viewPager.currentItem = 3
                            changeSelectedDrawerTile(DRAWER_TILE_GENRE)
                        }
                        R.id.drawerTilePlaylist -> {
                            viewPager.currentItem = 4
                            changeSelectedDrawerTile(DRAWER_TILE_PLAYLIST)
                        }
                        R.id.drawerTileFolder -> {
                            viewPager.currentItem = 5
                            changeSelectedDrawerTile(DRAWER_TILE_FOLDER)
                        }
                        R.id.drawerTileOfflineMusic -> {
                            changeRollBackSelectedDrawerTile(DRAWER_TILE_OFFLINE_MUSIC)
                            launchHomeActivity(this, true)
                        }
                        R.id.drawerTileOnlineMusic -> {
                            changeRollBackSelectedDrawerTile(DRAWER_TILE_ONLINE_MUSIC)
                            launchOnlineHomeActivity(this, true)
                        }
                        R.id.drawerTileFavorite -> {
                            changeRollBackSelectedDrawerTile(DRAWER_TILE_FAVORITE)
                            launchDetailFavoriteActivity(this)
                        }
                        R.id.drawerTileEqualizer -> {
                            changeRollBackSelectedDrawerTile(DRAWER_TILE_EQUALIZER)
                        }
                        R.id.drawerTileSleepTimer -> {
                            changeRollBackSelectedDrawerTile(DRAWER_TILE_SLEEP_TIMER)
                        }
                        R.id.drawerTileShare -> {
                            changeRollBackSelectedDrawerTile(DRAWER_TILE_SHARE)
                        }
                        R.id.drawerTileFeedback -> {
                            changeRollBackSelectedDrawerTile(DRAWER_TILE_FEEDBACK)
                        }
                        R.id.drawerTileWeAreSocial -> {
                            changeRollBackSelectedDrawerTile(DRAWER_TILE_WE_ARE_SOCIAL)
                        }
                        R.id.drawerTileSettings -> {
                            changeRollBackSelectedDrawerTile(DRAWER_TILE_SETTINGS)
                            openActivity(SettingActivity::class.java, null)
                        }
                    }
                }
            }
        }
        super.onClick(v)
    }

    override fun setLastState(music: BaseMusik) {
        setBackground(music)
        super.setLastState(music)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.option_sort -> showMenuSortDialog()
            R.id.option_search -> mSearchView.openSearch()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun createOptionMenu(menu: Menu?) {

    }

    override fun getOptionMenuId(): Int = R.menu.activity_home

    override fun initView() {
        with(this@BaseHomeActivity) {
            assignView()
            setupSearchView()
            super.initView()
            setupDrawer()
            setupActionBar()
            setupViewPager()
            mViewPager.addOnPageChangeListener(this)
        }
        // todo BottomNavigationView.OnNavigationItemSelectedListener will get called twice
        //  if both setupViewPager() and container.addOnPageChangeListener(this@HomeActivity) are called
        setupBottomNavigation()
        setViewOnClickListener(drawerTileAlbum, drawerTileSong, drawerTileArtist, drawerTileAlbum,
                drawerTileGenre, drawerTilePlaylist, drawerTileFavorite, drawerTileEqualizer,
                drawerTileSleepTimer, drawerTileShare, drawerTileFeedback, drawerTileWeAreSocial,
                drawerTileSettings
        )
//        drawerTileOfflineMusic.setOnClickListener(this)
//        drawerTileOnlineMusic.setOnClickListener(this)
    }

    override fun initData() {
        editMenu()
    }

    override fun blurView() {
        DynamicBlurUtils.blurView(
                this@BaseHomeActivity,
                rootViewId,
                mBlurToolbar,
                mBlurBottomBar,
                mBlurBottomNavigation
        )
    }

    override fun onBackPressed() {
        when {
            mDrawerLayout.isDrawerOpen(Gravity.RIGHT) -> {
                mDrawerLayout.closeDrawer(Gravity.RIGHT)
            }
            mSearchView.isOpen -> mSearchView.closeSearch()
            else -> super.onBackPressed()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            MaterialSearchView.REQUEST_VOICE -> {
                if (resultCode != Activity.RESULT_OK || data == null) return
                val matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                if (null == matches || matches.size == 0) return
                val searchWord = matches[0]
                if (TextUtils.isEmpty(searchWord)) return
                mSearchView.setQuery(searchWord, false)
            }
        }
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {
        if (position < 5) {
            selectBottomNavigationItem(position)
        }
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun logServiceConnected() {
        InformationLogUtils.logServiceConnected("BaseHomeActivity")
    }

    override fun logServiceDisconnected() {
        InformationLogUtils.logServiceDisconnected("BaseHomeActivity")
    }

    override fun updateSongInfo() {
        val music = mMusicService.playingSong
        setBackground(music)
        super.updateSongInfo()
    }

    abstract fun assignView()

    abstract fun editMenu()

    abstract fun setupViewPager()

    protected fun observeRecyclerCurrentPosition(vararg positionsLiveDatas: MutableLiveData<Int>) {
        positionsLiveDatas.forEach {
            it.observe(
                    this@BaseHomeActivity,
                    getToolBarScrollFlagObserver()
            )
        }
    }

    private fun setViewOnClickListener(vararg views: View) = views.forEach { it.setOnClickListener(this) }

    private fun updateLastSelectedHomeFragment() {
        lastSelectedHomeFragment = mHomeViewModel.getSelectedDrawerTile()
    }

    private fun setSelectedDrawerTile(selectedDrawerTile: String?) {
        mHomeViewModel.setSelectedDrawerTile(selectedDrawerTile)
    }

    private fun changeSelectedDrawerTile(selectedDrawerTile: String) {
        setSelectedDrawerTile(selectedDrawerTile)
        closeDrawer()
    }

    private fun changeRollBackSelectedDrawerTile(selectedDrawerTile: String) {
        updateLastSelectedHomeFragment()
        changeSelectedDrawerTile(selectedDrawerTile)
    }

    private fun closeDrawer() {
        mDrawerLayout.closeDrawer(Gravity.RIGHT)
    }

    private fun loadDefaultBackGround(imageView: ImageView) {
        ImageLoadingUtils.loadImage(imageView, R.drawable.bg_main_color)
    }

    private fun setupBottomNavigation() {
        with(mBottomNavigation) {
            setOnNavigationItemSelectedListener(mNavigationItemSelectedListener)
            setOnNavigationItemReselectedListener(mNavigationItemReselectedListener)
            itemIconTintList = null
        }
        selectBottomNavigationItem(1)
    }

    private fun setupActionBar() {
        with(mToolBar) {
            setSupportActionBar(this)
            supportActionBar?.run {
                setDisplayShowTitleEnabled(false)
                setHomeButtonEnabled(true)
                setDisplayHomeAsUpEnabled(true)
            }
            setNavigationOnClickListener {
                with(mDrawerLayout) {
                    if (isDrawerOpen(Gravity.RIGHT)) {
                        closeDrawer(Gravity.RIGHT)
                    } else {
                        openDrawer(Gravity.RIGHT)
                    }
                }
            }
        }
    }

    private fun setupDrawer() {
        mDrawerLayout.addDrawerListener(
                ActionBarDrawerToggle(
                        this,
                        mDrawerLayout,
                        mToolBar,
                        R.string.navigation_drawer_open,
                        R.string.navigation_drawer_close
                )
        )
    }

    private fun getToolBarScrollFlagObserver(): Observer<Int> {
        return Observer { currentPosition ->
            setToolBarScrollFlag(currentPosition)
        }
    }

    private fun setToolBarScrollFlag(currentPosition: Int) {
        if (currentPosition == 0) setPinToolbar()
        else setScrollToolBar()
    }

    private fun setupSearchView() {
        with(mSearchView) {
            setOnQueryTextListener(object : MaterialSearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    return true
                }

                override fun onQueryTextChange(newText: String): Boolean {
                    with(mHomeViewModel) {
                        setSearching(
                                if (newText.isNotEmpty()) {
                                    setSearchingText(newText)
                                    true
                                } else {
                                    setSearchingText(null)
                                    false
                                }
                        )
                    }
                    return true
                }
            })

            setSearchViewListener(object : MaterialSearchView.SearchViewListener {
                override fun onSearchViewOpened() {

                }

                override fun onSearchViewClosed() {

                }
            })

            setOnVoiceClickedListener { askSpeechInput() }
        }
    }

    private fun setBackground(music: BaseMusik?) {
        if (music == null) return
        val musicType = music.type
        if (TextUtils.isEmpty(musicType)) return
        when (musicType) {
            ConstantUtil.OFFLINE_MUSIC -> {
                val songLocation = music.location
                if (!TextUtils.isEmpty(songLocation)) {
                    with(mMetadataRetriever) {
                        setDataSource(songLocation)
                        val pictureBytes = embeddedPicture
                        if (pictureBytes != null) {
                            BlurImageUtils.blurImage(
                                    this@BaseHomeActivity,
                                    pictureBytes,
                                    mBackgroundImage
                            )
                        } else {
                            loadDefaultBackGround(mBackgroundImage)
                        }
                    }
                }
            }
            ConstantUtil.ONLINE_MUSIC -> {
                val coverArtLink = (music as OnlineMusik).coverArt
                if (!TextUtils.isEmpty(coverArtLink)) {
                    BlurImageUtils.blurImage(
                            this@BaseHomeActivity,
                            coverArtLink,
                            mRequestOptions,
                            mBackgroundImage
                    )
                } else {
                    loadDefaultBackGround(mBackgroundImage)
                }
            }
            else -> {
            }
        }
    }

    private fun askSpeechInput() {
        try {
            startActivityForResult(
                    Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH).apply {
                        putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
                        putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())
                        putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.speech_prompt))
                    },
                    MaterialSearchView.REQUEST_VOICE)
        } catch (a: ActivityNotFoundException) {
            a.printStackTrace()
            Toast.makeText(this,
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show()
        }
    }

    private fun selectBottomNavigationItem(position: Int) {
        with(mBottomNavigation.menu.getItem(position)) {
            isChecked = true
            mNavigationItemSelectedListener.onNavigationItemSelected(this)
        }
    }
}
