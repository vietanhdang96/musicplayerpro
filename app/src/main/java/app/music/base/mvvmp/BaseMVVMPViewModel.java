package app.music.base.mvvmp;

import androidx.lifecycle.ViewModel;

public abstract class BaseMVVMPViewModel<P extends MVVMPContract.Presenter>
        extends ViewModel implements MVVMPContract.ViewModel {

    protected P mPresenter;

    public BaseMVVMPViewModel(P mPresenter) {
        this.mPresenter = mPresenter;
        this.mPresenter.attachViewModel(this);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        mPresenter.detachViewModel();
    }
}
