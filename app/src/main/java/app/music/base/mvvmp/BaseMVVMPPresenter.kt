package app.music.base.mvvmp

abstract class BaseMVVMPPresenter<VM : MVVMPContract.ViewModel>
    : MVVMPContract.Presenter {

    protected var mViewModel: VM? = null

    override fun <ViewModelType : MVVMPContract.ViewModel?> attachViewModel(viewModel: ViewModelType) {
        mViewModel = viewModel as VM
    }

    override fun detachViewModel() {
        mViewModel = null
    }
}
