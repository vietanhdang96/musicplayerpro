package app.music.base.mvvmp;

public interface MVVMPContract {

    interface ViewModel {

    }

    interface Presenter {

        <ViewModelType extends ViewModel> void attachViewModel(ViewModelType viewModel);

        void detachViewModel();
    }
}
