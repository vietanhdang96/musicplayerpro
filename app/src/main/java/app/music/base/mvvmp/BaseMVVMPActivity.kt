package app.music.base.mvvmp

import androidx.databinding.Observable
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import app.music.base.activity.BaseActivity
import io.reactivex.disposables.Disposables
import javax.inject.Inject


abstract class BaseMVVMPActivity<B : ViewDataBinding, VM : MVVMPContract.ViewModel>
    : BaseActivity<B>() {

    @Inject
    protected lateinit var mViewModel: VM

    protected fun <T : Observable> T.addOnPropertyChanged(callback: (T) -> Unit) =
            object : Observable.OnPropertyChangedCallback() {
                override fun onPropertyChanged(observable: Observable?, i: Int) =
                        callback(observable as T)
            }.also { addOnPropertyChangedCallback(it) }
                    .let { Disposables.fromAction { removeOnPropertyChangedCallback(it) } }

    protected fun <T : Any?> observeLiveData(liveData: MutableLiveData<T>, observer: Observer<T>) {
        liveData.observe(this@BaseMVVMPActivity, observer)
    }
}
