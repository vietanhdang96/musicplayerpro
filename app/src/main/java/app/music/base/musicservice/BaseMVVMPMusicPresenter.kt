package app.music.base.musicservice

import android.app.Activity
import android.content.*
import android.media.MediaMetadataRetriever
import android.os.Handler
import android.os.IBinder
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import app.music.base.mvvmp.BaseMVVMPPresenter
import app.music.model.entity.BaseMusik
import app.music.service.MusicService
import app.music.utils.ConstantUtil
import app.music.utils.broadcast.MusicBroadcast
import com.google.android.exoplayer2.ExoPlayer
import java.util.*
import javax.inject.Inject

abstract class BaseMVVMPMusicPresenter<VM : MVVMPMusicContract.ViewModel>
    : BaseMVVMPPresenter<VM>(),
        MVVMPMusicContract.Presenter {

    protected var mMusicService: MusicService? = null
    private val mMusicBroadcast by lazy { mViewModel?.run { MusicBroadcast(::setBufferingMusic) } }
    protected var mMusicBound = false
    protected lateinit var mRunnable: Runnable
    protected lateinit var mMusicBufferStatusRunnable: Runnable
    protected var mOldNotificationState: Int = 0
    protected var mOldLocation: String? = null
    protected var mLastMusicList: List<BaseMusik> = ArrayList()
    protected var mLastPlayedMusicObject: BaseMusik? = null
    var mOldPlayerState: Boolean = false
    @Inject
    protected lateinit var mHandler: Handler
    @Inject
    protected lateinit var mPlayIntent: Intent
    @Inject
    protected lateinit var mMetadataRetriever: MediaMetadataRetriever
    private var mMusicConnection: ServiceConnection = object : ServiceConnection {

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            mMusicService = (service as MusicService.MusicBinder).service
            mMusicBound = true
        }

        override fun onServiceDisconnected(name: ComponentName) {
            mMusicBound = false
        }
    }

    override fun unregisterMusicBroadcast(context: Context) {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(mMusicBroadcast!!)
    }

    override fun registerMusicBroadcast(context: Context) {
        LocalBroadcastManager
                .getInstance(context)
                .registerReceiver(mMusicBroadcast!!, IntentFilter(ConstantUtil.MUSIC_ACTION))
    }

    override fun displayPlayingSongInfo(
            dismissNotification: () -> Unit, loadLastState: () -> Unit, updateSongInfo: () -> Unit,
            setButtonImageResource: () -> Unit, setSeekBarProgress: (Int) -> Unit) {
        processPlayerState(
                dismissNotification,
                loadLastState,
                updateSongInfo,
                null,
                setButtonImageResource,
                setSeekBarProgress
        )
    }

    override fun displayPlayingSongInfo(
            dismissNotification: () -> Unit, loadLastState: () -> Unit, updateSongInfo: () -> Unit,
            displayRotateAnimation: (Boolean) -> Unit, setButtonImageResource: () -> Unit,
            setSeekBarProgress: (Int) -> Unit) {
        processPlayerState(
                dismissNotification,
                loadLastState,
                updateSongInfo,
                displayRotateAnimation,
                setButtonImageResource,
                setSeekBarProgress
        )
    }

    private fun processPlayerState(
            dismissNotification: () -> Unit, loadLastState: () -> Unit, updateSongInfo: () -> Unit,
            displayRotateAnimation: ((Boolean) -> Unit)?, setButtonImageResource: () -> Unit,
            setSeekBarProgress: (Int) -> Unit) {
        mRunnable = Runnable {
            if (mMusicService != null && mMusicService?.isRunning!!) {
                mMusicService?.run {
                    if (mMusicBound && exoPlayer != null) {
                        when (notificationState) {
                            -1 -> {
                                if (mOldNotificationState != notificationState) {
                                    if (isPlaying) {
                                        dismissNotification()
                                    } else {
                                        loadLastState()
                                    }
                                    mOldNotificationState = -1
                                }
                            }
                            1 -> {
                                mOldNotificationState = 1
                                if (mOldLocation != getPlayingSongLocation()) {
                                    mOldLocation = getPlayingSongLocation()
                                    updateSongInfo()
                                    mOldPlayerState = !isPlaying
                                }
                                if (mOldPlayerState != isPlaying) {
                                    displayRotateAnimation?.invoke(isPlaying)
                                    setButtonImageResource()
                                    mOldPlayerState = isPlaying
                                }
                                setSeekBarProgress(playerCurrentPosition.toInt())
                            }
                        }
                    }
                }
                mHandler.postDelayed(mRunnable, 1000)
            } else {
                loadLastState()
            }
        }
        updateMusicBufferStatus()
        checkEndSong()
    }

    override fun changeExoPlayerState(
            context: Context, pauseMusic: () -> Unit, playMusic: () -> Unit,
            getSeekBarProgress: () -> Long, setReplayAndShuffleImageResource: (() -> Unit)?) {
        mMusicService?.run {
            if (isRunning) {
                if (!mMusicBound || exoPlayer == null) return
                if (isPlaying) {
                    pauseMusic()
                } else {
                    playMusic()
                }
            } else {
                setList(mLastMusicList)
                setSong(mLastPlayedMusicObject!!)
                startService(context)
                seekTime = getSeekBarProgress()
                playSong()
                mOldLocation = ""
                checkEndSong()
                setPlayerShuffleMode()
                setPlayerRepeatMode()
                setReplayAndShuffleImageResource?.invoke()
            }
        }
    }

    override fun resetOldState() {
        mOldLocation = ""
        mOldNotificationState = 0
    }

    override fun bindMusicService(activity: Activity) {
        activity.bindService(mPlayIntent, mMusicConnection, Context.BIND_AUTO_CREATE)
    }

    override fun unbindMusicService(activity: Activity) {
        activity.unbindService(mMusicConnection)
    }

    override fun removeHandlerCallBack() {
        mHandler.removeCallbacksAndMessages(null)
    }

    override fun playPreviousSong(context: Context) {
        mMusicService?.run {
            switchToOtherSong(context, ::playPrev)
        }
    }

    override fun playNextSong(context: Context) {
        mMusicService?.run {
            switchToOtherSong(context, ::playNext)
        }
    }

    private fun updateMusicBufferStatus() {
        mMusicBufferStatusRunnable = Runnable {
            mMusicService?.run {
                if (isRunning && mMusicBound) {
                    exoPlayer?.run {
                        mViewModel?.setBufferingMusic(playbackState == ExoPlayer.STATE_BUFFERING)
                        mHandler.removeCallbacks(mMusicBufferStatusRunnable)
                    }
                }
            }
        }
        mHandler.postDelayed(mMusicBufferStatusRunnable, 100)
    }

    private fun switchToOtherSong(context: Context, playOther: () -> Unit) {
        mMusicService?.run {
            if (isRunning) {
                startService(context)
                playOther()
            } else {
                setList(mLastMusicList)
                setSong(mLastPlayedMusicObject!!)
                playOther()
                startService(context)
                playSong()
                checkEndSong()
            }
            setPlayerShuffleMode()
            setPlayerRepeatMode()
        }
    }

    private fun checkEndSong() = mHandler.post(mRunnable)

    override fun startService(context: Context) {
        mPlayIntent.action = ConstantUtil.ACTION.STARTFOREGROUND_ACTION
        ContextCompat.startForegroundService(context, mPlayIntent)
    }
}
