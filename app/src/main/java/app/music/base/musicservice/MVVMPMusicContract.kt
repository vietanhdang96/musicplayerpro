package app.music.base.musicservice

import android.app.Activity
import android.content.Context
import androidx.databinding.ObservableBoolean
import app.music.base.mvvmp.MVVMPContract

interface MVVMPMusicContract {

    interface ViewModel : MVVMPContract.ViewModel {

        var mIsBufferingMusic: ObservableBoolean

        fun setPlaying(isPlaying: Boolean)

        fun setSongTitle(songTitle: String)

        fun setArtistName(artistName: String)

        fun resetOldState()

        fun removeHandlerCallBack()

        fun unbindMusicService(activity: Activity)

        fun bindMusicService(activity: Activity)

        fun setSeekBarMax(seekBarMax: Int)

        fun setSeekBarProgress(seekBarProgress: Int)

        fun getSeekBarProgress(): Int

        fun setBufferingMusic(isBufferingMusic: Boolean)

        fun unregisterMusicBroadcast(context: Context)

        fun registerMusicBroadcast(context: Context)
    }

    interface Presenter : MVVMPContract.Presenter {

        fun playPreviousSong(context: Context)

        fun playNextSong(context: Context)

        fun bindMusicService(activity: Activity)

        fun unbindMusicService(activity: Activity)

        fun resetOldState()

        fun removeHandlerCallBack()

        fun displayPlayingSongInfo(
                dismissNotification: () -> Unit, loadLastState: () -> Unit, updateSongInfo: () -> Unit,
                setButtonImageResource: () -> Unit, setSeekBarProgress: (Int) -> Unit)

        fun displayPlayingSongInfo(
                dismissNotification: () -> Unit, loadLastState: () -> Unit, updateSongInfo: () -> Unit,
                displayRotateAnimation: (Boolean) -> Unit, setButtonImageResource: () -> Unit,
                setSeekBarProgress: (Int) -> Unit)

        fun changeExoPlayerState(
                context: Context, pauseMusic: () -> Unit, playMusic: () -> Unit,
                getSeekBarProgress: () -> Long, setReplayAndShuffleImageResource: (() -> Unit)?)

        fun startService(context: Context)

        fun unregisterMusicBroadcast(context: Context)

        fun registerMusicBroadcast(context: Context)
    }
}