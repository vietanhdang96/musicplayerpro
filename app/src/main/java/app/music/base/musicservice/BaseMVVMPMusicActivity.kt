package app.music.base.musicservice

import android.app.Activity
import androidx.databinding.ViewDataBinding
import app.music.base.mvvmp.BaseMVVMPActivity
import kotlinx.android.synthetic.main.activity_detail_play_music.*

abstract class BaseMVVMPMusicActivity<
        B : ViewDataBinding, VM : MVVMPMusicContract.ViewModel>
    : BaseMVVMPActivity<B, VM>() {

    override fun onStart() {
        super.onStart()
        bindMusicService()
        with(mViewModel) {
            resetOldState()
            mCompositeDisposable.add(
                    mIsBufferingMusic.addOnPropertyChanged {
                        with(animationViewLoading) {
                            if (mIsBufferingMusic.get()) {
                                playAnimation()
                            } else {
                                cancelAnimation()
                            }
                        }
                    })
            registerMusicBroadcast(this@BaseMVVMPMusicActivity)
        }
    }

    override fun onStop() {
        super.onStop()
        with(mViewModel) {
            removeHandlerCallBack()
            with(this@BaseMVVMPMusicActivity) {
                unbindMusicService(this)
                unregisterMusicBroadcast(this)
            }
        }
    }

    protected fun bindMusicService() {
        mViewModel.bindMusicService(this)
    }

    protected fun unbindMusicService(activity: Activity) {
        mViewModel.unbindMusicService(activity)
    }
}
