package app.music.base.musicservice

import android.app.Activity
import android.content.Context
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import app.music.base.mvvmp.BaseMVVMPViewModel

abstract class BaseMVVMPMusicViewModel<P : MVVMPMusicContract.Presenter>(presenter: P)
    : BaseMVVMPViewModel<P>(presenter),
        MVVMPMusicContract.ViewModel {

    var mSongTitle = ObservableField<String>()
    var mArtistName = ObservableField<String>()
    var mIsPlaying = ObservableBoolean()
    var mSeekBarProgress = ObservableInt(0)
    var mSeekBarMax = ObservableInt(0)
    override var mIsBufferingMusic = ObservableBoolean(false)

    override fun unregisterMusicBroadcast(context: Context) {
        mPresenter.unregisterMusicBroadcast(context)
    }

    override fun registerMusicBroadcast(context: Context) {
        mPresenter.registerMusicBroadcast(context)
    }

    override fun setSeekBarMax(seekBarMax: Int) {
        mSeekBarMax.set(seekBarMax)
    }

    override fun setSeekBarProgress(seekBarProgress: Int) {
        mSeekBarProgress.set(seekBarProgress)
    }

    override fun getSeekBarProgress(): Int {
        return mSeekBarProgress.get()
    }

    override fun setPlaying(isPlaying: Boolean) {
        mIsPlaying.set(isPlaying)
    }

    override fun setBufferingMusic(isBufferingMusic: Boolean) {
        mIsBufferingMusic.set(isBufferingMusic)
    }

    override fun setSongTitle(songTitle: String) {
        mSongTitle.set(songTitle)
    }

    override fun setArtistName(artistName: String) {
        mArtistName.set(artistName)
    }

    override fun bindMusicService(activity: Activity) {
        mPresenter.bindMusicService(activity)
    }

    override fun unbindMusicService(activity: Activity) {
        mPresenter.unbindMusicService(activity)
    }

    override fun resetOldState() {
        mPresenter.resetOldState()
    }

    override fun removeHandlerCallBack() {
        mPresenter.removeHandlerCallBack()
    }
}
