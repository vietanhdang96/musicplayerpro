package app.music.utils.sharepreferences

import android.content.SharedPreferences
import android.text.TextUtils
import app.music.model.entity.ListContainer
import com.google.gson.Gson
import java.util.*
import javax.inject.Inject

class PreferencesHelper @Inject constructor(
        private val mPreferences: SharedPreferences,
        private val mPreferencesEditor: SharedPreferences.Editor,
        private val mGson: Gson) {

    fun remove(key: String) {
        mPreferencesEditor.remove(key)
        apply()
    }

    fun getLong(key: String): Long = mPreferences.getLong(key, 0)

    fun getLong(key: String, defaultValue: Long): Long = mPreferences.getLong(key, defaultValue)

    fun getInt(key: String, defaultValue: Int): Int = mPreferences.getInt(key, defaultValue)

    fun getString(key: String): String = mPreferences.getString(key, "").toString()

    fun getString(key: String, defaultValue: String): String {
        return mPreferences.getString(key, defaultValue).toString()
    }

    fun putString(key: String, value: String) {
        mPreferencesEditor.putString(key, value)
    }

    fun putBoolean(key: String, value: Boolean) {
        mPreferencesEditor.putBoolean(key, value)
    }

    fun getBoolean(key: String, defaultValue: Boolean): Boolean {
        return mPreferences.getBoolean(key, defaultValue)
    }

    fun putInt(key: String, value: Int) {
        mPreferencesEditor.putInt(key, value)
    }

    fun <T, C : ListContainer<*>> getList(
            containerClassName: Class<C>, valueKey: String): MutableList<T> {
        val valueString = mPreferences.getString(valueKey, "")
        var dataList: MutableList<T> = ArrayList()
        if (TextUtils.isEmpty(valueString)) return dataList
        val container = Gson().fromJson(valueString, containerClassName)
        val tempList = container.dataList
        if (null == tempList || tempList.isEmpty()) return dataList
        dataList = ArrayList((tempList as Collection<T>?)!!)
        return dataList
    }

    fun <T : ListContainer<*>> putList(key: String, container: T) {
        val value = mGson.toJson(container)
        putString(key, value)
    }

    fun apply() = mPreferencesEditor.apply()
}