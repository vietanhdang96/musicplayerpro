package app.music.utils.dialog.songoption

import app.music.model.entity.BaseMusik
import app.music.utils.listener.dialoglistener.DialogSongOptionListener

object DialogSongOptionMethodUtils {

    fun showSongOption(dialogSongOptionListener: DialogSongOptionListener, musik: BaseMusik) {
        dialogSongOptionListener.showSongOptionDialog(musik)
    }
}