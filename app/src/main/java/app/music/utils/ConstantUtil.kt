package app.music.utils

object ConstantUtil {
    const val SPAN_COUNT_THREE = 3
    const val MY_PERMISSION_REQUEST = 1
    const val PLAYING_NOTIFICATION_ID = 1
    const val DYNAMIC_BLUR_RADIUS = 20f
    const val ONLINE_MUSIC = "ONLINE_MUSIC"
    const val OFFLINE_MUSIC = "OFFLINE_MUSIC"

    const val MUSIC_ACTION = "gitlab.com/vietanhdang96/musicplayerpro.onlinemusicbuffer.action"
    const val MUSIC_BROADCAST_MESSAGE = "MUSIC_BROADCAST_MESSAGE"
    const val ONLINE_MUSIC_BUFFERING = "ONLINE_MUSIC_BUFFERING"
    const val ONLINE_MUSIC_READY = "ONLINE_MUSIC_READY"

    interface ACTION {
        companion object {
            const val MAIN_ACTION = "customnotification.action.main"
            const val INIT_ACTION = "customnotification.action.init"
            const val PREV_ACTION = "customnotification.action.prev"
            const val PLAY_ACTION = "customnotification.action.play"
            const val NEXT_ACTION = "customnotification.action.next"
            const val STARTFOREGROUND_ACTION = "customnotification.action.startforeground"
            const val STOPFOREGROUND_ACTION = "customnotification.action.stopforeground"
        }
    }

    interface NOTIFICATION_ID {
        companion object {
            const val FOREGROUND_SERVICE = 101
        }
    }
}