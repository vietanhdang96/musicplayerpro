package app.music.utils.listener

interface RecyclerScrollToTopListener {
    fun onScrollToTop()
}