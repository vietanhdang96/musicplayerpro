package app.music.utils.listener.homefragmentlistener

interface FolderFragmentListener {

    fun onSortFolder(sortBy: String, isAscending: String)
}
