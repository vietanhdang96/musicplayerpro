package app.music.utils.listener.homefragmentlistener

interface PlaylistFragmentListener {
    fun onSortPlaylist(sortBy: String, isAscending: String)
}
