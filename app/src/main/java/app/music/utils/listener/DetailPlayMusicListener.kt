package app.music.utils.listener

interface DetailPlayMusicListener {
    fun changePlayButtonImageResource(resId: Int)

    fun changeExoPlayerState()
}
