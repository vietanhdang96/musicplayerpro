package app.music.utils.listener.homefragmentlistener

interface ArtistFragmentListener {
    fun onSortArtist(sortBy: String, isAscending: String)
}
