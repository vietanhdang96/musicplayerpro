package app.music.utils.listener

import androidx.coordinatorlayout.widget.CoordinatorLayout
import app.music.base.home.BaseHomeViewModel
import app.music.screen.offlinehome.HomeViewModel
import com.google.android.material.appbar.AppBarLayout
import eightbitlab.com.blurview.BlurView

interface ToolbarScrollFlagListener<VM : BaseHomeViewModel> {

    var mBlurToolbar: BlurView
    var mAppBarLayout: AppBarLayout?
    var mHomeViewModel: VM

    fun setAlbumRecyclerCurrentPosition(position: Int) {
        mHomeViewModel.setAlbumRecyclerCurrentPosition(position)
    }

    fun setSongRecyclerCurrentPosition(position: Int) {
        mHomeViewModel.setSongRecyclerCurrentPosition(position)
    }

    fun setArtistRecyclerCurrentPosition(position: Int) {
        mHomeViewModel.setArtistRecyclerCurrentPosition(position)
    }

    fun setGenreRecyclerCurrentPosition(position: Int) {
        mHomeViewModel.setGenreRecyclerCurrentPosition(position)
    }

    fun setPlaylistRecyclerCurrentPosition(position: Int) {
        mHomeViewModel.setPlaylistRecyclerCurrentPosition(position)
    }

    fun setFolderRecyclerCurrentPosition(position: Int) {
        (mHomeViewModel as HomeViewModel).setFolderRecyclerCurrentPosition(position)
    }

    fun setPinToolbar() {
        with(getToolbarLayoutParam()) {
            if (scrollFlags != 0) {
                mAppBarLayout?.layoutParams = getAppBarLayoutParam().apply {
                    scrollFlags = 0
                    behavior = null
                }
            }
        }
    }

    fun setScrollToolBar() {
        with(getToolbarLayoutParam()) {
            if (scrollFlags != getToolBarScrollFlag()) {
                mAppBarLayout?.layoutParams = getAppBarLayoutParam().apply {
                    scrollFlags = getToolBarScrollFlag()
                    behavior = AppBarLayout.Behavior()
                }
            }
        }
    }

    fun getToolBarScrollFlag(): Int {
        return AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or
                AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS or
                AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP
    }

    fun getToolbarLayoutParam(): AppBarLayout.LayoutParams {
        return mBlurToolbar.layoutParams as AppBarLayout.LayoutParams
    }

    fun getAppBarLayoutParam(): CoordinatorLayout.LayoutParams {
        return mAppBarLayout?.layoutParams as CoordinatorLayout.LayoutParams
    }
}
