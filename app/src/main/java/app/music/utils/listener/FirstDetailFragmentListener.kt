package app.music.utils.listener

interface FirstDetailFragmentListener {
    fun changeCenterCoverArt(byteArray: ByteArray)

    fun changeCenterCoverArt(coverArtLink: String)

    fun resumeAnimation()

    fun pauseAnimation()

    fun startAnimation()

    fun changePlayButtonActivate(buttonActivate: Boolean)
}
