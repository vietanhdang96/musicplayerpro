package app.music.utils.listener.homefragmentlistener

interface GenreFragmentListener {
    fun onSortGenre(sortBy: String, isAscending: String)
}
