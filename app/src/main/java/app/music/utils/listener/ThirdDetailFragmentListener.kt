package app.music.utils.listener

interface ThirdDetailFragmentListener {
    fun updateLyrics(lyrics: String)
}
