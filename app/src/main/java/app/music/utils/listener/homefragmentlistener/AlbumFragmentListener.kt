package app.music.utils.listener.homefragmentlistener

interface AlbumFragmentListener {
    fun onSortAlbum(sortBy: String, orderBy: String)
}
