package app.music.utils.listener.homefragmentlistener

interface SongFragmentListener {

    fun onSortMusic(sortBy: String, isAscending: String)

//    fun onFilterList(filterPattern: String)
}
