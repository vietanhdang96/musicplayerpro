package app.music.utils.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import app.music.utils.ConstantUtil

class MusicBroadcast(private val onSetLoadingIndicatorVisibility: (Boolean) -> Unit)
    : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        with(ConstantUtil) {
            onSetLoadingIndicatorVisibility(
                    intent?.getStringExtra(MUSIC_BROADCAST_MESSAGE) == ONLINE_MUSIC_BUFFERING
            )
        }
    }
}