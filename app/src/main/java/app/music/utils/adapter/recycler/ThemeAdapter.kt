package app.music.utils.adapter.recycler

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import app.music.R
import app.music.databinding.ItemThemeBinding
import app.music.model.entity.Theme
import app.music.utils.viewholder.ThemeViewHolder
import java.lang.ref.WeakReference


class ThemeAdapter(
        private val contextReference: WeakReference<Context>,
        private var mThemeList: MutableList<Theme>,
        private var mSelectedThemeIndex: Int,
        private val onSetThemeList: (MutableList<Theme>, Theme) -> Unit,
        private val onSetThemeIndex: (Int) -> Unit,
        private val onSetIconBackResource: (String, String) -> Unit)
    : RecyclerView.Adapter<ThemeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ThemeViewHolder {
        val binding = DataBindingUtil.inflate<ItemThemeBinding>(
                LayoutInflater.from(parent.context),
                R.layout.item_theme,
                parent,
                false)
        return ThemeViewHolder(
                WeakReference(contextReference.get() as Activity),
                binding,
                ::themeClickListener,
                onSetIconBackResource
        )
    }

    override fun getItemCount(): Int = mThemeList.size

    override fun onBindViewHolder(holder: ThemeViewHolder, position: Int) {
        holder.bindData(mThemeList[position])
    }

    private fun themeClickListener(newSelectedItem: Int) {
        if (mSelectedThemeIndex != newSelectedItem) {
            with(mThemeList[mSelectedThemeIndex]) {
                isSelected = !isSelected
            }
            with(mThemeList[newSelectedItem]) {
                isSelected = !isSelected
            }
            notifyItemChanged(mSelectedThemeIndex)
            mSelectedThemeIndex = newSelectedItem
            onSetThemeList(mThemeList, mThemeList[newSelectedItem])
            onSetThemeIndex(mSelectedThemeIndex)
        }
    }
}
