package app.music.utils.adapter.recycler

import android.app.Activity
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.music.R
import app.music.databinding.ItemHomeSecondFragmentBinding
import app.music.model.entity.BaseMusik
import app.music.utils.diffcallback.FilterDiffCallBack
import app.music.utils.diffcallback.SongDiffCallBack
import app.music.utils.listener.ToolbarScrollFlagListener
import app.music.utils.viewholder.MusicViewHolder
import java.lang.ref.WeakReference

class SongAdapter(
        mActivityWeakReference: WeakReference<Activity>,
        private var itemClickListeners: (BaseMusik) -> Unit,
        private var itemLongClickListeners: (BaseMusik) -> Unit)
    : BaseHomeRecyclerAdapter<BaseMusik, MusicViewHolder>(mActivityWeakReference) {

    override val layoutId = R.layout.item_home_second_fragment

    override fun getViewHolder(binding: ViewDataBinding): MusicViewHolder {
        return MusicViewHolder(
                mActivityReference,
                binding as ItemHomeSecondFragmentBinding,
                itemClickListeners,
                itemLongClickListeners
        )
    }

    override fun getDiffResult(
            isFilter: Boolean, dataList: List<BaseMusik>, newItems: List<BaseMusik>): DiffUtil.DiffResult {
        return DiffUtil.calculateDiff(
                if (isFilter)
                    FilterDiffCallBack(dataList, newItems)
                else
                    SongDiffCallBack(dataList, newItems),
                false
        )
    }

    override fun isContainingFilterPatternItem(item: BaseMusik, filterPattern: String): Boolean {
        return item.title.toLowerCase().contains(filterPattern)
    }

    override fun getScrollListener(recyclerView: RecyclerView) {
        val layoutManager = recyclerView.layoutManager as LinearLayoutManager
        (mActivityReference.get() as ToolbarScrollFlagListener<*>)
                .setSongRecyclerCurrentPosition(layoutManager.findFirstVisibleItemPosition())
    }
}
