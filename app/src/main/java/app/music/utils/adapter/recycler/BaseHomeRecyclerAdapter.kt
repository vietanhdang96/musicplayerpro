package app.music.utils.adapter.recycler

import android.app.Activity
import androidx.recyclerview.widget.RecyclerView
import app.music.base.BaseRecyclerAdapter
import app.music.base.BaseViewHolder
import java.lang.ref.WeakReference


abstract class BaseHomeRecyclerAdapter<T, H : BaseViewHolder<T, *>>
(mActivityReference: WeakReference<Activity>)
    : BaseRecyclerAdapter<T, H>(mActivityReference) {

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                getScrollListener(recyclerView)
            }
        })
    }

    abstract fun getScrollListener(recyclerView: RecyclerView)
}
