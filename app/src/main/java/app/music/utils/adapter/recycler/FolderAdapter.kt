package app.music.utils.adapter.recycler

import android.app.Activity
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.music.R
import app.music.databinding.ItemHomeSixthFragmentBinding
import app.music.model.entity.Folder
import app.music.utils.diffcallback.FilterDiffCallBack
import app.music.utils.diffcallback.FolderDiffCallBack
import app.music.utils.listener.ToolbarScrollFlagListener
import app.music.utils.viewholder.FolderViewHolder
import java.lang.ref.WeakReference

class FolderAdapter(
        mActivityWeakReference: WeakReference<Activity>,
        private var itemClickListeners: (Folder) -> Unit,
        private var itemLongClickListeners: (Folder) -> Unit)
    : BaseHomeRecyclerAdapter<Folder, FolderViewHolder>(mActivityWeakReference) {

    override val layoutId = R.layout.item_home_sixth_fragment

    override fun getViewHolder(binding: ViewDataBinding): FolderViewHolder {
        return FolderViewHolder(
                mActivityReference,
                binding as ItemHomeSixthFragmentBinding,
                itemClickListeners,
                itemLongClickListeners
        )
    }

//    override fun getItemClickListener(activity: Activity): Any {
//        return object : FolderItemClickListener {
//            override fun onFolderClick(folder: Folder, isLongClick: Boolean) {
//                (activity as FolderItemClickListener).onFolderClick(folder, isLongClick)
//            }
//        }
//    }

    override fun getDiffResult(
            isFilter: Boolean, dataList: List<Folder>, newItems: List<Folder>): DiffUtil.DiffResult {
        return DiffUtil.calculateDiff(
                if (isFilter) FilterDiffCallBack(dataList, newItems)
                else FolderDiffCallBack(dataList, newItems),
                false)
    }

    override fun isContainingFilterPatternItem(item: Folder, filterPattern: String): Boolean {
        return item.folderName.toLowerCase().contains(filterPattern)
    }

    override fun getScrollListener(recyclerView: RecyclerView) {
        val layoutManager = recyclerView.layoutManager as LinearLayoutManager
        (mActivityReference.get() as ToolbarScrollFlagListener<*>)
                .setFolderRecyclerCurrentPosition(layoutManager.findFirstVisibleItemPosition())
    }
}
