package app.music.utils.adapter.recycler

import android.app.Activity
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.music.R
import app.music.databinding.ItemHomeFirstFragmentBinding
import app.music.model.entity.Album
import app.music.utils.diffcallback.AlbumDiffCallBack
import app.music.utils.diffcallback.FilterDiffCallBack
import app.music.utils.listener.ToolbarScrollFlagListener
import app.music.utils.viewholder.AlbumViewHolder
import java.lang.ref.WeakReference


class AlbumAdapter(
        mActivityWeakReference: WeakReference<Activity>,
        private var itemClickListeners: (Album) -> Unit,
        private var itemLongClickListeners: (Album) -> Unit)
    : BaseHomeRecyclerAdapter<Album, AlbumViewHolder>(mActivityWeakReference) {

    override val layoutId: Int = R.layout.item_home_first_fragment

    override fun getDiffResult(isFilter: Boolean, dataList: List<Album>, newItems: List<Album>)
            : DiffUtil.DiffResult {
        return DiffUtil.calculateDiff(
                if (isFilter) FilterDiffCallBack(dataList, newItems)
                else AlbumDiffCallBack(dataList, newItems),
                false)
    }

    override fun getViewHolder(binding: ViewDataBinding): AlbumViewHolder {
        return AlbumViewHolder(
                mActivityReference,
                binding as ItemHomeFirstFragmentBinding,
                itemClickListeners,
                itemLongClickListeners
        )
    }

    override fun isContainingFilterPatternItem(item: Album, filterPattern: String): Boolean {
        return item.albumName.toLowerCase().contains(filterPattern)
    }

    override fun getScrollListener(recyclerView: RecyclerView) {
        val layoutManager = recyclerView.layoutManager as GridLayoutManager
        (mActivityReference.get() as ToolbarScrollFlagListener<*>)
                .setAlbumRecyclerCurrentPosition(layoutManager.findFirstVisibleItemPosition())
    }
}
