package app.music.utils.threadhandler.workmanager

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import app.music.utils.log.InformationLogUtils
import app.music.utils.musicloading.LoadMusicUtil
import app.music.utils.sort.SortMethodUtils

class OfflineMusicWorker(private var context: Context, workerParams: WorkerParameters)
    : Worker(context, workerParams) {

    val TAG = "OfflineMusicWorker"
    private var mContext: Context = context

    override fun doWork(): Result {
        return try {
            InformationLogUtils.logMusicThreadStart(TAG)
            LoadMusicUtil.getMusic(mContext)
            SortMethodUtils.sortSongList(context)
            val outputData = workDataOf("GET_MUSIC_FINISHED" to true)
            Result.success(outputData)
        } catch (e: Exception) {
            Result.failure()
        }
    }
}