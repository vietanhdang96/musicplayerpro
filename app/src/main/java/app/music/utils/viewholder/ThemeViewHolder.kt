package app.music.utils.viewholder

import android.app.Activity
import android.content.Context
import android.text.TextUtils
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.recyclerview.widget.RecyclerView
import app.music.R
import app.music.databinding.ItemThemeBinding
import app.music.model.entity.Theme
import java.lang.ref.WeakReference

class ThemeViewHolder(
        private val activityReference: WeakReference<Activity>,
        private val itemViewBinding: ItemThemeBinding,
        private val themeClickListeners: (Int) -> Unit,
        private val onSetIconBackResource: (String, String) -> Unit)
    : RecyclerView.ViewHolder(itemViewBinding.root) {

    var themeName = ObservableField<String>()
    var background = ObservableInt()
    var isSelectedTheme = ObservableBoolean()

    fun bindData(dataObject: Theme) {
        with(itemViewBinding) {
            if (itemview == null) {
                itemview = this@ThemeViewHolder
            }
            with(isSelectedTheme) {
                set(dataObject.isSelected)
                with(root) {
                    setOnClickListener {
                        set(true)
                        themeClickListeners(adapterPosition)
                    }
                }
            }
        }
        dataObject.let {
            val context by lazy { activityReference.get() as Context }
            val defaultValue by lazy { context.getString(R.string.empty_string) }
            background.set(it.background)
            themeName.set(
                    if (TextUtils.isEmpty(it.mThemeTitle)) defaultValue
                    else it.mThemeTitle
            )
            if (it.isSelected) {
                onSetIconBackResource(it.toolbarDarkThemeTextColor, it.toolbarLightThemeTextColor)
            }
        }
    }
}
