package app.music.utils;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;

import app.music.R;
import app.music.utils.color.AttributeColorUtils;

public class DataBindingAdapter {

    @BindingAdapter("android:src")
    public static void setImageUri(ImageView view, String imageUri) {
        if (imageUri == null) {
            view.setImageURI(null);
        } else {
            view.setImageURI(Uri.parse(imageUri));
        }
    }

    @BindingAdapter("android:src")
    public static void setImageUri(ImageView view, Uri imageUri) {
        view.setImageURI(imageUri);
    }

    @BindingAdapter("android:src")
    public static void setImageDrawable(ImageView view, Drawable drawable) {
        view.setImageDrawable(drawable);
    }

    @BindingAdapter("android:src")
    public static void setImageResource(ImageView imageView, int resource) {
        imageView.setImageResource(resource);
    }

    @BindingAdapter({"drawerTileTextColor"})
    public static void characterBackground(TextView textView, boolean isSelected) {
        textView.setTextColor(
                isSelected
                        ? Color.parseColor("#ffffff")
                        : AttributeColorUtils
                        .getColor(textView.getContext(), R.attr.recycler_text_color)
        );
    }
}
