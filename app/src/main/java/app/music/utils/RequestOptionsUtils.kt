package app.music.utils

import android.os.Build
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.request.RequestOptions

object RequestOptionsUtils {

    fun getRequestOptions(resourceId: Int, overrideWidth: Int, overrideHeight: Int): RequestOptions {
        val mIsAndroidO: Boolean = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        val mDecodeFormat: DecodeFormat = if (mIsAndroidO) DecodeFormat.PREFER_ARGB_8888 else DecodeFormat.PREFER_RGB_565
        val mRequestOptions = RequestOptions()
                .error(resourceId)
                .override(overrideWidth, overrideHeight)
                .centerCrop()
                .format(mDecodeFormat)
        if (mIsAndroidO) mRequestOptions.disallowHardwareConfig()
        return mRequestOptions
    }
}