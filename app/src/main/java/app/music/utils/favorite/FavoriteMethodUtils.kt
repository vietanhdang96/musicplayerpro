package app.music.utils.favorite

import android.content.Context
import android.text.TextUtils
import app.music.model.entity.BaseMusik
import app.music.model.entity.Music
import app.music.model.entity.OnlineMusik
import app.music.utils.ConstantUtil
import app.music.utils.sharepreferences.SharedPrefMethodUtils
import com.google.gson.Gson
import com.google.gson.JsonParser
import java.lang.ref.WeakReference
import java.util.*

object FavoriteMethodUtils {

    private val mGson = Gson()

    fun getFavoriteList(contextReference: WeakReference<Context>): List<BaseMusik> {
        val sharedPreferences = SharedPrefMethodUtils.getSharedPreferences(contextReference)
        val valueString = sharedPreferences.getString(FavoriteConstantUtils.PREF_FAVORITE_LIST, "")
        val dataList = ArrayList<BaseMusik>()
        if (TextUtils.isEmpty(valueString)) return dataList
        val jsonValue = JsonParser().parse(valueString!!) ?: return dataList
        val musicDataArray = jsonValue.asJsonArray
        if (musicDataArray == null || musicDataArray.size() == 0) return dataList

        musicDataArray.forEach { jsonElement ->
            jsonElement?.let {
                if (jsonElement.toString().contains(ConstantUtil.OFFLINE_MUSIC)) {
                    dataList.add(mGson.fromJson(jsonElement, Music::class.java))
                } else {
                    dataList.add(mGson.fromJson(jsonElement, OnlineMusik::class.java))
                }
            }
        }
        return dataList
    }
}
