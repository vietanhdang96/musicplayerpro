package app.music.utils.imageloading

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.media.MediaMetadataRetriever
import android.text.TextUtils
import android.widget.ImageView
import app.music.R
import app.music.model.entity.BaseMusik
import app.music.model.entity.OnlineMusik
import app.music.utils.ConstantUtil
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.ViewTarget
import com.bumptech.glide.request.transition.Transition
import io.github.armcha.coloredshadow.ShadowImageView

object ImageLoadingUtils {

    fun loadImage(imageView: ImageView, imageData: ByteArray?, requestOptions: RequestOptions) {
        Glide.with(imageView)
                .load(imageData)
                .apply(requestOptions)
                .centerCrop()
                .into(imageView)
    }

    fun loadImage(imageView: ImageView, imageData: Bitmap, requestOptions: RequestOptions) {
        Glide.with(imageView)
                .load(imageData)
                .apply(requestOptions)
                .into(imageView)
    }

    fun loadImage(imageView: ImageView, imageLink: String, requestOptions: RequestOptions) {
        Glide.with(imageView)
                .load(imageLink)
                .apply(requestOptions)
                .into(imageView)
    }

    fun loadImage(imageView: ImageView, resourceId: Int?) {
        Glide.with(imageView)
                .load(resourceId)
                .into(imageView)
    }

    fun loadMusicImage(baseMusik: BaseMusik, metadataRetriever: MediaMetadataRetriever,
                       requestOptions: RequestOptions, imageView: ImageView) {
        val musicType = baseMusik.type
        if (TextUtils.isEmpty(musicType)) return
        when (musicType) {
            ConstantUtil.OFFLINE_MUSIC -> {
                with(metadataRetriever) {
                    setDataSource(baseMusik.location)
                    loadImage(imageView, embeddedPicture, requestOptions)
                }
            }
            ConstantUtil.ONLINE_MUSIC -> {
                val musicLocation = (baseMusik as OnlineMusik).coverArt
                if (!TextUtils.isEmpty(musicLocation)) {
                    loadImage(imageView, musicLocation, requestOptions)
                }
            }
        }
    }

    fun getCoverArtRequestOption(): RequestOptions {
        return RequestOptions().error(R.drawable.ic_disc_56dp)
    }

    fun loadShadowedImage(imageView: ShadowImageView, imageBytes: ByteArray,
                          requestOptions: RequestOptions) {
        with(imageView) {
            Glide.with(this)
                    .load(imageBytes)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .apply(requestOptions)
                    .into(object : ViewTarget<ImageView, Drawable>(this) {
                        override fun onLoadStarted(placeholder: Drawable?) {
                            super.onLoadStarted(placeholder)
                            setImageDrawable(placeholder)
                        }

                        override fun onLoadCleared(placeholder: Drawable?) {
                            super.onLoadCleared(placeholder)
                            setImageDrawable(placeholder)
                        }

                        override fun onLoadFailed(errorDrawable: Drawable?) {
                            super.onLoadFailed(errorDrawable)
                            setImageDrawable(errorDrawable)
                        }

                        override fun onResourceReady(
                                resource: Drawable, transition: Transition<in Drawable>?) {
                            setImageDrawable(resource)
                        }
                    })
        }
    }

    fun loadShadowedImage(imageView: ShadowImageView, imageUrl: String,
                          requestOptions: RequestOptions) {
        with(imageView) {
            Glide.with(this)
                    .load(imageUrl)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .apply(requestOptions)
                    .into(object : ViewTarget<ImageView, Drawable>(this) {
                        override fun onLoadStarted(placeholder: Drawable?) {
                            super.onLoadStarted(placeholder)
                            setImageDrawable(placeholder)
                        }

                        override fun onLoadCleared(placeholder: Drawable?) {
                            super.onLoadCleared(placeholder)
                            setImageDrawable(placeholder)
                        }

                        override fun onLoadFailed(errorDrawable: Drawable?) {
                            super.onLoadFailed(errorDrawable)
                            setImageDrawable(errorDrawable)
                        }

                        override fun onResourceReady(
                                resource: Drawable, transition: Transition<in Drawable>?) {
                            setImageDrawable(resource)
                        }
                    })
        }
    }
}
