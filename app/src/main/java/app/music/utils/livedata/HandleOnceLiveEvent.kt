package app.music.utils.livedata

/**
 * Used as a wrapper for data that is exposed via a LiveData that represents an event.
 */
open class HandleOnceLiveEvent<out T>(private val value: T) {

    private var hasBeenHandled = false

    /**
     * Returns the content and prevents its use again.
     */
    fun getValueIfNotHandled(): T? {
        return if (hasBeenHandled) {
            null
        } else {
            hasBeenHandled = true
            value
        }
    }

    fun getValue(): T? = value
}