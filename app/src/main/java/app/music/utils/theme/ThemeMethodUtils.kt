package app.music.utils.theme

import android.content.Context
import app.music.R
import app.music.utils.AppDataUtils
import app.music.utils.sharepreferences.SharedPrefMethodUtils
import java.lang.ref.WeakReference

object ThemeMethodUtils {

    fun setAppTheme(context: Context) {
        if (AppDataUtils.sIsDarkModeEnabled) {
            setDarkTheme(context)
        } else {
            setLightTheme(context)
        }
    }

    private fun setDarkTheme(context: Context) {
        context.setTheme(
                when (AppDataUtils.sThemeIndex) {
                    0 -> R.style.DarkThemeMaroon
                    1 -> R.style.DarkThemeDarkRed
                    2 -> R.style.DarkThemeBrown
                    3 -> R.style.DarkThemeDeepSkyBlue
                    4 -> R.style.DarkThemeNavy
                    5 -> R.style.DarkThemeIndigo
                    6 -> R.style.DarkThemeMagenta
                    7 -> R.style.DarkThemeBlack
                    else -> R.style.DarkThemeBlack
                })
    }

    private fun setLightTheme(context: Context) {
        context.setTheme(
                when (AppDataUtils.sThemeIndex) {
                    0 -> R.style.LightThemeMaroon
                    1 -> R.style.LightThemeDarkRed
                    2 -> R.style.LightThemeBrown
                    3 -> R.style.LightThemeDeepSkyBlue
                    4 -> R.style.LightThemeNavy
                    5 -> R.style.LightThemeIndigo
                    6 -> R.style.LightThemeMagenta
                    7 -> R.style.LightThemeBlack
                    else -> R.style.LightThemeBlack
                })
    }

    fun getDarkModeEnabled(contextReference: WeakReference<Context>): Boolean {
        val sharedPreferences = SharedPrefMethodUtils.getSharedPreferences(contextReference)
        return sharedPreferences.getBoolean(ThemeConstantUtils.PREF_IS_DARK_MODE_ENABLED, true)
    }

    fun getThemeIndex(contextReference: WeakReference<Context>): Int {
        val sharedPreferences = SharedPrefMethodUtils.getSharedPreferences(contextReference)
        return sharedPreferences.getInt(ThemeConstantUtils.PREF_THEME_INDEX, 0)
    }
}
