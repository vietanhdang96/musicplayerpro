package app.music.utils.theme

object ThemeConstantUtils {

    const val PREF_IS_DARK_MODE_ENABLED = "PREF_IS_DARK_MODE_ENABLED"
    const val PREF_THEME_INDEX = "PREF_CURRENT_THEME_INDEX"
    const val PREF_THEME_LIST = "PREF_THEME_LIST"
}
