package app.music.utils

import app.music.model.entity.Theme

object AppDataUtils {

    var sThemeList = mutableListOf<Theme>()
    var sThemeIndex = 0
    var sIsDarkModeEnabled = true
}