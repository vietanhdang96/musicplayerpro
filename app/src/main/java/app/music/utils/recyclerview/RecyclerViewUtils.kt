package app.music.utils.recyclerview

import android.content.Context
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.music.R

object RecyclerViewUtils {

    fun setVerticalLinearLayout(context: Context, recyclerView: RecyclerView?) {
        recyclerView?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    }

    fun setVerticalLinearLayout(context: Context, recyclerView: RecyclerView?, hasFixedSize: Boolean) {
        recyclerView?.setHasFixedSize(hasFixedSize)
        setVerticalLinearLayout(context, recyclerView)
    }

    fun setVerticalLinearLayout(context: Context, recyclerView: RecyclerView, hasFixedSize: Boolean,
                                hasDividerItemDecoration: Boolean) {
        setVerticalLinearLayout(context, recyclerView, hasFixedSize)
        recyclerView.addItemDecoration(
                DividerItemDecoration(context, DividerItemDecoration.VERTICAL).apply {
                    setDrawable(ContextCompat.getDrawable(context, R.drawable.custom_divider)!!)
                }
        )
    }
}
